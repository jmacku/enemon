#!/usr/bin/env python

from signal import signal, SIGINT
from twisted.internet import reactor, defer
from lib.tools import check_rpi
import lib.http_api as http_api
import lib.config.defaults as defaults
from lib.factory import Factory
from lib.project import (
    Action_001,
    Action_002,
    Action_003,
    Action_004,
    Action_005,
    Action_006,
    Action_007,
    Action_008,
    Action_009,
    Action_010
)


def handler(signal_received, frame):
    global factory
    print('Ctrl-C pressed!')
    factory.set_global_stop()


@defer.inlineCallbacks
def prepare():
    yield factory.factory_start()
    for action_id, action in factory.action_list.action_list.items():
        yield action.init_action()


@defer.inlineCallbacks
def run():
    for action_id, action in factory.action_list.action_list.items():
        yield action.run_action()


# ----------------------------------------------
if __name__ == '__main__':
    factory = Factory(defaults.REDIS_HOST)
    if check_rpi():
        action_001 = Action_001(factory)
    action_002 = Action_002(factory)
    action_003 = Action_003(factory)
    action_004 = Action_004(factory)
    action_005 = Action_005(factory)
    action_006 = Action_006(factory)
    action_007 = Action_007(factory)
    action_008 = Action_008(factory)
    action_009 = Action_009(factory)
    action_010 = Action_010(factory)

    signal(SIGINT, handler)
    prepare()
    run()

    reactor.listenTCP(8080,
                      http_api.create_httpapi_controller(
                          factory.get_redis_helper()))

    print('Starting Enclient application.')
    reactor.run()
    print('Enclient application stopped.')
