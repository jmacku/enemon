#!/usr/bin/env python

from twisted.internet import reactor, defer
from twisted.internet.threads import deferToThread

from periphery import SPI, GPIO
import lib.config.defaults as defaults


class RpIODigital(object):
    def __init__(self, addr, direction, edge=None):
        self.addr = addr
        self.direction = direction
        self.edge = edge
        self.io = None

    @defer.inlineCallbacks
    def io_open(self):
        gpio = yield deferToThread(
            GPIO, "/dev/gpiochip0",
            self.addr,
            'in' if self.direction == defaults.DIR_INPUT else 'out'
        )
        self.io = gpio
        if self.edge:
            self.io.edge = self.edge

    @defer.inlineCallbacks
    def io_close(self):
        yield deferToThread(self.io.close)

    @defer.inlineCallbacks
    def io_read(self):
        if self.direction == defaults.DIR_OUTPUT:
            return None
        val = yield deferToThread(self.io.read)
        defer.returnValue(int(val))

    @defer.inlineCallbacks
    def io_write(self, value):
        if self.direction == defaults.DIR_INPUT:
            return
        yield deferToThread(self.io.write, bool(value))

    def io_read_all_events(self):
        event = None
        while (self.io.poll(0)):
            event = self.io.read_event()
        return event

    @defer.inlineCallbacks
    def io_read_event(self):
        if not self.edge:
            defer.returnValue(None)
        val = yield deferToThread(self.io_read_all_events)
        defer.returnValue(val)


class RpIOAnalog(object):
    def __init__(self, direction=defaults.DIR_INPUT):
        self.direction = direction
        self.io = None

    @defer.inlineCallbacks
    def io_open(self):
        self.io = yield deferToThread(SPI, "/dev/spidev0.0", 0,
                                      defaults.SPI_CLOCK)

    @defer.inlineCallbacks
    def io_close(self):
        yield deferToThread(self.io.close)

    def spi_request(self, addr):
        request = [0x01, 0x80, 0x00]  # specific to MCP3008
        request[1] += addr << 4
        return request

    @defer.inlineCallbacks
    def io_read(self, addr):
        r = yield deferToThread(
            self.io.transfer,
            self.spi_request(addr)
        )
        val = (((r[1] & 0x03) << 8) + r[2])
        defer.returnValue(val)

    @defer.inlineCallbacks
    def io_write():
        pass


class RpOneWire(object):
    def __init__(self, path):
        self.file_path = path
        self.sensor_data = None

    def read_file_content(self, file_path):
        try:
            with open(file_path, 'r') as file:
                content = file.read()
                return content
        except Exception as e:
            return str(e)

    @defer.inlineCallbacks
    def get_sensor_data(self):
        data = yield deferToThread(
            self.read_file_content,
            self.file_path
        )
        defer.returnValue(data)

    def io_read(self):
        pass


class RpDalas(RpOneWire):
    def __init__(self, path):
        super().__init__(path)

    @defer.inlineCallbacks
    def io_read(self):
        data = yield self.get_sensor_data()
        temp_lines = data.splitlines()
        if len(temp_lines) != 2:
            defer.returnValue(999)
        temp_split = temp_lines[1].split('t=')
        if len(temp_split) != 2:
            defer.returnValue(999)
        temp_mil = temp_split[1]
        try:
            temp = float(temp_mil) / 1000.0
        except Exception as e:
            print(e)
            defer.returnValue(999)
        defer.returnValue(temp)


# =================================================================
# Tests
@defer.inlineCallbacks
def main():
    print('Digital input')
    di = RpIODigital(23, defaults.DIR_INPUT)
    yield di.io_open()
    val = yield di.io_read()
    print(val)
    yield di.io_close()
    print('Digital output')
    do = RpIODigital(21, defaults.DIR_OUTPUT)
    yield do.io_open()
    yield do.io_write(True)
    yield do.io_close()
    print('Analog input')
    ai = RpIOAnalog()
    yield ai.io_open()
    val = yield ai.io_read(1)
    print(val)
    yield ai.io_close()
    term = RpDalas('/sys/bus/w1/devices/28-00000c08902f/w1_slave')
    # term = RpDalas('temp_test')
    t_data = yield term.io_read()
    print(f"Temperature: {t_data}")


if __name__ == '__main__':
    reactor.callLater(5, reactor.stop)
    main()
    print('Test start')
    reactor.run()
    print('Test stop')
