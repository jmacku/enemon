#!/usr/bin/env python
from twisted.internet import defer
from lib.redis_api import RedisHelper
from lib.action_manager import ActionList

class Factory:
    def __init__(self, redis_host, redis_port=6379):
        self.redis_helper = RedisHelper(redis_host, redis_port)
        self.global_stop = False
        self.action_list = ActionList()

    @defer.inlineCallbacks
    def factory_start(self):
        try:
            yield self.redis_helper.connect_redis()
        except Exception as error:
            print(f'Factory start: {error}')

    @defer.inlineCallbacks
    def factory_stop(self):
        try:
            yield self.redis_helper.disconnect_redis()
        except Exception as error:
            print(f'Factory storp: {error}')
        pass

    def get_redis_helper(self):
        return self.redis_helper

    def set_global_stop(self):
        self.global_stop = True
        self.action_list.quit_all()

    def reset_global_stop(self):
        self.global_stop = False

    def get_global_stop(self):
        return self.global_stop
