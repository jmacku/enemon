#!/usr/bin/env python

from twisted.internet import reactor, defer

from lib.varlist import VarList
from lib.rp_io import RpIODigital, RpIOAnalog, RpDalas
# from lib.redis_api import RedisHelper
from lib.factory import Factory
import lib.config.defaults as defaults
from datetime import datetime


class RpIOInterface(object):
    def __init__(self, var_list):
        self.var_list = var_list
        self.analog_io = None

    @defer.inlineCallbacks
    def init_io(self):
        spi_initialized = False
        for var in self.var_list.var_list:
            if var.get_var_type() in (
                    defaults.TYPE_DIGITAL,
                    defaults.TYPE_EDGE):
                io = RpIODigital(var.get_var_addr(), var.get_var_dir(),
                                 var.get_var_edge())
                yield io.io_open()
                var.io = io
            if var.get_var_type() == defaults.TYPE_ANALOG:
                if not spi_initialized:
                    spi_initialized = True
                    self.analog_io = RpIOAnalog()
                    yield self.analog_io.io_open()
                var.io = self.analog_io
            if var.get_var_type() == defaults.TYPE_DALAS:
                self.analog_io = RpDalas(var.get_var_addr())
                var.io = self.analog_io

    @defer.inlineCallbacks
    def close_io(self):
        spi_closed = False
        for var in self.var_list.var_list:
            if var.get_var_type() == defaults.TYPE_DIGITAL:
                yield var.io.io_close()
            if var.get_var_type() == defaults.TYPE_ANALOG:
                if not spi_closed:
                    spi_closed = True
                    yield self.analog_io.io_close()

    @defer.inlineCallbacks
    def read_io(self):
        for var in self.var_list.var_list:
            if var.get_var_type() == defaults.TYPE_DIGITAL:
                if var.get_var_dir() == defaults.DIR_INPUT:
                    val = yield var.io.io_read()
                    var.set_var_value(val)
            if var.get_var_type() == defaults.TYPE_ANALOG:
                if var.get_var_dir() == defaults.DIR_INPUT:
                    val = yield var.io.io_read(var.get_var_addr())
                    var.set_var_value(val)
            if var.get_var_type() == defaults.TYPE_DALAS:
                if var.get_var_dir() == defaults.DIR_INPUT:
                    val = yield var.io.io_read()
                    var.set_var_value(val)
            if var.get_var_type() == defaults.TYPE_EDGE:
                if var.get_var_dir() == defaults.DIR_INPUT:
                    val = yield var.io.io_read_event()
                    if val:
                        ev_epoch = val.timestamp//1000000000
                        var_tmstp = datetime.utcfromtimestamp(
                                        ev_epoch).strftime('%Y%m%d%H%M%S')
                        var_suffix = var_tmstp[:-4]
                        var.set_var_value(var_tmstp)
                        var.set_suffix(var_suffix)
                    else:
                        var.set_var_value(None)

    @defer.inlineCallbacks
    def write_io(self):
        for var in self.var_list.var_list:
            if var.get_var_type() == defaults.TYPE_DIGITAL:
                if var.get_var_dir() == defaults.DIR_OUTPUT:
                    yield var.io.io_write(var.get_var_value())


class RpIOEngine(RpIOInterface):
    def __init__(self, factory):
        self.var_list = VarList()
        self.var_list.add_var("TANK_STOP", 20, defaults.TYPE_DIGITAL,
                              defaults.DIR_OUTPUT)
        self.var_list.add_var("PUMP_COMM", 21, defaults.TYPE_DIGITAL,
                              defaults.DIR_OUTPUT)
        self.var_list.add_var("WL_LOW_PIN", 23, defaults.TYPE_DIGITAL,
                              defaults.DIR_INPUT)
        self.var_list.add_var("WL_MID_PIN", 24, defaults.TYPE_DIGITAL,
                              defaults.DIR_INPUT)
        self.var_list.add_var("WL_HIGH_PIN", 25, defaults.TYPE_DIGITAL,
                              defaults.DIR_INPUT)
        self.var_list.add_var("TEMP_RAW", 0, defaults.TYPE_ANALOG,
                              defaults.DIR_INPUT)
        self.var_list.add_var("TEMP_DAS01",
                              '/sys/bus/w1/devices/28-00000d16f4c4/w1_slave',
                              defaults.TYPE_DALAS,
                              defaults.DIR_INPUT)
        self.var_list.add_var("RAIN_GAUGE_PULSE",
                              defaults.RAIN_GAUGE_INPUT,
                              defaults.TYPE_EDGE,
                              defaults.DIR_INPUT, var_ttl=3*3600,
                              var_edge=defaults.EDGE_FALLING)
        self.factory = factory
        super().__init__(self.var_list)

    @defer.inlineCallbacks
    def get_values_from_redis(self):
        values = yield self.factory.get_redis_helper().read_redis(
            [x.get_var_name() for x in self.var_list.iter_vars(
                defaults.DIR_OUTPUT, defaults.ALL_TYPES_EXC_EDGE)]
        )
        for val, item in zip(
                values, (
                    x for x in self.var_list.iter_vars(
                        defaults.DIR_OUTPUT, defaults.ALL_TYPES_EXC_EDGE)
                )):
            item.set_var_value(val)

    @defer.inlineCallbacks
    def set_values_to_redis(self):
        yield self.factory.get_redis_helper().write_redis(
            [x.get_var_name() for x in self.var_list.iter_vars(
                defaults.DIR_INPUT, defaults.ALL_TYPES_EXC_EDGE)],
            [x.get_var_value() for x in self.var_list.iter_vars(
                defaults.DIR_INPUT, defaults.ALL_TYPES_EXC_EDGE)]
        )
        yield self.factory.get_redis_helper().write_edge_redis(
            [x.get_var_name() for x in self.var_list.iter_vars(
                defaults.DIR_INPUT, (defaults.TYPE_EDGE, ))],
            [x.get_var_value() for x in self.var_list.iter_vars(
                defaults.DIR_INPUT, (defaults.TYPE_EDGE, ))],
            [x.get_var_ttl() for x in self.var_list.iter_vars(
                defaults.DIR_INPUT, (defaults.TYPE_EDGE, ))]
        )

    @defer.inlineCallbacks
    def io_loop(self):
        yield self.get_values_from_redis()
        yield self.read_io()
        yield self.write_io()
        yield self.set_values_to_redis()


# ========================================================================
# Tests
if __name__ == '__main__':
    from twisted.internet import task
    from signal import signal, SIGINT

    factory = Factory('loalhost', 6378)
    factory.factory_start()
    rp_engine = RpIOEngine(factory)

    def handler(signal_received, frame):
        global factory
        print('Ctrl-C pressed!')
        factory.set_global_stop()

    @defer.inlineCallbacks
    def prepare():
        global rp_engine
        yield rp_engine.init_io()

    @defer.inlineCallbacks
    def cyclic_task_0(factory):
        global rp_engine

        yield rp_engine.io_loop()
        if factory.get_global_stop():
            raise Exception('Loop 0 stopped')

    @defer.inlineCallbacks
    def finish(Err=None):
        print(Err)
        global rp_engine, factory
        yield rp_engine.close_io()
        yield factory.factory_stop()
        reactor.stop()

    signal(SIGINT, handler)
    prepare()
    loop1 = task.LoopingCall(cyclic_task_0, factory)
    d1 = loop1.start(1.0, now=False)
    d1.addCallback(finish)
    d1.addErrback(finish)

    print('Test start')
    reactor.run()
    # print(f'stop_loop: {stop_loop}')
    print('Test stop')
