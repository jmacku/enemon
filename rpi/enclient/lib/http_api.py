#!/usr/bin/env python

from twisted.internet import reactor, defer
from twisted.web.resource import Resource, NoResource
from twisted.web import server

from lib.redis_api import RedisHelper
import lib.config.defaults as defaults
import json

class CmdRes(object):
    _OK = 200
    _ERR = 400

    def __init__(self):
        self.set()

    def set(self, msg = 'ok', res_code = -1, is_json = False):
        if res_code < 0:
            self.code = self._OK
        else:
            self.code = res_code
        if is_json:
            self.msg = msg
        else:
            self.msg = json.dumps({"result": msg})


class HttpApiModule(Resource):
    """ """
    isLeaf = True
    def __init__(self, redis_helper):
        Resource.__init__(self)
        self.redis_helper = redis_helper

    @defer.inlineCallbacks
    def get_var_val(self, var_names):
        values = yield self.redis_helper.read_redis(var_names)
        return values

    @defer.inlineCallbacks
    def set_var_val(self, var_names, values):
        yield self.redis_helper.write_redis(var_names, values)

    def write_response(self, request, res=None):
        if res is None:
            res = CmdRes()
        request.responseHeaders.addRawHeader(b'content-type',
                b'application/json')
        request.setResponseCode(res.code)
        request.write(res.msg.encode())
        request.finish()

    def render_GET(self, request):
        self.res = CmdRes()
        if request.path.decode() == '/api-get':
            self.get_var(request)
        elif request.path.decode() == '/api-set':
            self.set_var(request)
        else:
            self.res.set(f'error: unknown command {request.path}', CmdRes._ERR)
            self.write_response(request, self.res)
        return server.NOT_DONE_YET

    @defer.inlineCallbacks
    def get_var(self, request):
        result = []
        all_found = True
        try:
            names = [n.decode() for n in request.args[b'name']]
            values = yield self.get_var_val(names)
            for pair in zip(names, values):
                result.append(dict([pair]))
            self.res.set(json.dumps(result), CmdRes._OK, True)
        except Exception as error:
            self.res.set(f'error: {error}', CmdRes._ERR)
        finally:
            self.write_response(request, self.res)

    @defer.inlineCallbacks
    def set_var(self, request):
        result = []
        all_found = True
        try:
            names = [n.decode() for n in request.args.keys()]
            values = [v[0].decode() for v in request.args.values()]
            yield self.set_var_val(names, values)
            self.res.set(json.dumps({"cmd": "ok"}), CmdRes._OK, True)
        except Exception as error:
            self.res.set(f'error: ({error})', CmdRes._ERR)
        finally:
            self.write_response(request, self.res)

def httpapi_factory(redis_helper):
    return HttpApiModule(redis_helper)

def create_httpapi_controller(redis_helper, resourceFactory = httpapi_factory):
    root = resourceFactory(redis_helper)
    root.putChild('api-set', resourceFactory(redis_helper))
    root.putChild('api-get', resourceFactory(redis_helper))
    command = server.Site(root)
    return command
