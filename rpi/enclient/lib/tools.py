#!/usr/bin/env python

def check_rpi():
    with open('/proc/cpuinfo') as f:
        for line in f:
            line = line.strip()
            if line.startswith('Model'):
                if 'Raspberry' in line:
                    return True
                else:
                    return False
    return False
