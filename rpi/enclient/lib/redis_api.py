#!/usr/bin/env python3

from twisted.internet import reactor, defer
import txredisapi as redis

_DEFAULT_NS = 'ns1'


class KeyName(object):
    """
    """
    def __init__(self, ns=_DEFAULT_NS, suffix=None):
        self.ns = ns
        self.suffix = suffix

    def add_ns(self, name):
        nsname = self.ns + ':' + name if self.ns else name
        return nsname

    def strip_ns(self, name):
        sname = name.split(':')[0] if self.ns else name
        return sname


class RedisHelper(object):

    """
    """
    def __init__(self, nodeid, port=6379, dbid=0):
        self.nodeid = nodeid
        self.port = port
        self.dbid = dbid
        self.connected = False
        self.redis_handler = None
        self.keyName = KeyName()

    @defer.inlineCallbacks
    def connect_redis(self):
        """
        """
        try:
            self.redis_handler = yield redis.Connection(
                self.nodeid,
                self.port,
                self.dbid
            )
            self.connected = True
        except Exception as error:
            print(error)

    @defer.inlineCallbacks
    def disconnect_redis(self):
        """
        """
        try:
            yield self.redis_handler.disconnect()
            self.connect = False
        except Exception as error:
            print(error)

    @defer.inlineCallbacks
    def write_redis(self, names, vals):
        """
        """
        if len(names) != len(vals):
            print("The len of names and values does not match!")
            return
        try:
            pipe = yield self.redis_handler.pipeline()
            for name, val in zip(names, vals):
                nsname = self.keyName.add_ns(name)
                pipe.hset(nsname, "value", val)
            yield pipe.execute_pipeline()
        except Exception as error:
            print(error)

    @defer.inlineCallbacks
    def write_edge_redis(self, names, vals, ttls):
        """
        """
        if len(names) != len(vals):
            print("The len of names and values does not match!")
            return
        try:
            pipe = yield self.redis_handler.pipeline()
            for name, val, ttl in zip(names, vals, ttls):
                if val:
                    nsname = self.keyName.add_ns(name)
                    if ttl:
                        pipe.set(nsname, '', expire=ttl,
                                 only_if_not_exists=True)
                    pipe.append(nsname, f'{val}|')
            yield pipe.execute_pipeline()
        except Exception as error:
            print(error)

    @defer.inlineCallbacks
    def read_redis(self, names):
        """
        """
        if not len(names):
            # print("Nothing to do, empty list!")
            return []
        try:
            pipe = yield self.redis_handler.pipeline()
            for name in names:
                nsname = self.keyName.add_ns(name)
                pipe.hget(nsname, "value")
            res = yield pipe.execute_pipeline()
            defer.returnValue(res)
        except Exception as error:
            print(error)
            defer.returnValue(None)

    @defer.inlineCallbacks
    def read_redis_edge(self, names):
        """
        """
        if not len(names):
            # print("Nothing to do, empty list!")
            return []
        try:
            pipe = yield self.redis_handler.pipeline()
            for name in names:
                nsname = self.keyName.add_ns(name)
                pipe.get(nsname)
            res = yield pipe.execute_pipeline()
            res_clean = [val if val else '0' for val in res]
            defer.returnValue(res_clean)
        except Exception as error:
            print(error)
            defer.returnValue(None)

    @defer.inlineCallbacks
    def key_exist_redis(self, names, def_vals):
        """
        """
        if not len(names):
            # print("Nothing to do, empty list!")
            return []
        for name, def_val in zip(names, def_vals):
            nsname = self.keyName.add_ns(name)
            try:
                bo_exists = yield self.redis_handler.exists(nsname)
            except Exception as error:
                print(error)
            if not bo_exists:
                try:
                    yield self.redis_handler.hset(nsname, "value", def_val)
                except Exception as error:
                    print(error)


# ========================================================
# Tests
# ========================================================
@defer.inlineCallbacks
def test(host, port):
    redis_helper = RedisHelper(host, port)
    yield redis_helper.connect_redis()
    yield redis_helper.write_redis(['p0', 'p1', 'p2'], [1, 2, 3])
    ll = yield redis_helper.read_redis(['p0', 'p1', 'p2'])
    print(f'Read from Redis {str(ll)}')
    yield redis_helper.write_redis(['p0', 'p1', 'p2'], [4, 5, 6])
    ll = yield redis_helper.read_redis(['p0', 'p1', 'p2'])
    print(f'Read from Redis {str(ll)}')
    yield redis_helper.write_redis(['p0', 'p1', 'p2'], [7, 8, 9])
    ll = yield redis_helper.read_redis(['p0', 'p1', 'p2'])
    print(f'Read from Redis {str(ll)}')
    yield redis_helper.disconnect_redis()


if __name__ == '__main__':
    host = 'localhost'
    port = 6379

    print('Test start')
    reactor.callLater(5, reactor.stop)
    test(host, port)
    reactor.run()
    print('Test stop')
