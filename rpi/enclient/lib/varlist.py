#!/usr/bin/env python3
import lib.config.defaults as defaults


class VarItem(object):
    def __init__(self, var_name, var_addr, var_type, var_dir, var_def_value=0,
                 var_scale=1.0, var_ttl=None, var_edge=None):
        self.var_name = var_name
        self.var_addr = var_addr
        self.var_type = var_type
        self.var_dir = var_dir
        self.var_scale = var_scale
        self.var_def_value = var_def_value
        self.var_value = var_def_value
        self.var_ttl = var_ttl
        self.var_edge = var_edge
        self.io = None
        self.suffix = None

    def get_var_name(self):
        return f'{self.var_name}:{self.suffix}' if self.suffix else f'{self.var_name}'

    def get_var_addr(self):
        return self.var_addr

    def get_var_type(self):
        return self.var_type

    def get_var_dir(self):
        return self.var_dir

    def get_var_ttl(self):
        return self.var_ttl

    def get_var_edge(self):
        return self.var_edge

    def get_var_value(self):
        return self.var_value

    def get_var_def_value(self):
        return self.var_def_value

    def set_var_value(self, value):
        self.var_value = value

    def set_var_ttl(self, ttl):
        self.var_ttl = ttl

    def set_suffix(self, suffix):
        self.suffix = suffix


class VarList(object):
    def __init__(self):
        self.var_list = []

    def add_var(self, var_name, var_addr, var_type, var_dir, var_def_value=0,
                var_scale=1.0, var_ttl=None, var_edge=None):
        self.var_list.append(
            VarItem(var_name, var_addr, var_type, var_dir, var_def_value,
                    var_scale, var_ttl, var_edge)
        )

    def get_by_name(self, var_name):
        for item in self.var_list:
            if item.get_var_name() == var_name:
                return item
        return None

    def iter_vars(self, var_dir, type_list=defaults.ALL_TYPES):
        for item in self.var_list:
            if (item.get_var_dir() & var_dir) != 0 and (
                    item.get_var_type() in type_list):
                yield item

# --------------------------------------------------
# Tests


if __name__ == "__main__":

    var_list = VarList()
    var_list.add_var("PUMP", 21, defaults.TYPE_DIGITAL, defaults.DIR_OUTPUT)
    var_list.add_var("LEVEL_1", 23, defaults.TYPE_DIGITAL, defaults.DIR_INPUT)
    var_list.add_var("LEVEL_2", 24, defaults.TYPE_DIGITAL, defaults.DIR_INPUT)
    var_list.add_var("LEVEL_3", 25, defaults.TYPE_DIGITAL, defaults.DIR_INPUT)
    var_list.add_var("TEMP", 0, defaults.TYPE_ANALOG, defaults.DIR_INPUT)
    it = var_list.iter_vars(defaults.DIR_INPUT)
    while True:
        try:
            v = it.__next__()
            print(f'Input: {v.get_var_name()}')
        except (StopIteration):
            print('No more items')
            break
    it = var_list.iter_vars(defaults.DIR_OUTPUT)
    while True:
        try:
            v = it.__next__()
            print(f'Output: {v.get_var_name()}')
        except (StopIteration):
            print('No more items')
            break
    s = (x for x in var_list.iter_vars(defaults.DIR_INPUT))
    print("Input variables:")
    for p in s:
        print(p.get_var_name())
