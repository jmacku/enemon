#!/usr/bin/env python

from twisted.internet import task
from twisted.internet import reactor, defer

import lib.config.defaults as defaults


class Action:
    def __init__(self, action_id, action_list, period, delay=0):
        self.period = period
        self.delay = delay
        self.action_id = action_id
        self.action_list = action_list
        self.handle = None
        if self.action_list:
            self.action_list.add_action(self.action_id, self)

    def action_job(self, *args, **kwargs):
        raise NotImplementedError('Subclass needs to implement this!')

    def init_action(self, *args, **kwargs):
        print(f'Initializing action #{self.action_id}')
        pass

    def run_action(self, *args, **kwargs):
        self.handle = task.LoopingCall(self.action_job, *args, **kwargs)
        self.d = self.handle.start(self.period, now=False)
        self.d.addCallback(self.on_success)
        self.d.addErrback(self.on_failure)

    def finish(self):
        pass

    def quit(self):
        print(f'Stopping task #{self.action_id}.')
        if self.handle.running:
            self.handle.stop()
            print(f'Task #{self.action_id} stopped.')

    def on_success(self, msg):
        print(f'Task id {self.action_id} succeeded.')
        self.finish()
        if self.action_list.all_stopped():
            print('Action list empty, stopping the reactor')
            reactor.stop()

    def on_failure(self, err):
        print(f'Task id {self.action_id} failed with error: {err.getErrorMessage()}.')
        self.finish()
        if self.action_list.all_stopped():
            print('Action list empty, stopping the reactor')
            reactor.stop()


class ActionList:
    def __init__(self):
        self.action_list = {}

    def action_exists(self, action_id):
        return action_id in self.action_list

    def add_action(self, action_id, action):
        if not self.action_exists(action_id):
            self.action_list[action_id] = action

    def del_action(self, action_id):
        if self.action_exists(action_id):
            self.action_list.pop(action_id)

    def is_empty(self):
        return len(self.action_list) == 0

    def all_stopped(self):
        boTmp = all(action.handle.running == False for action in self.action_list.values())
        return boTmp

    def quit_all(self):
        for action_id, action in self.action_list.items():
            action.quit()

#--------------------------------------------------------------
def main():
    pass

if __name__ == '__main__':
    main()
