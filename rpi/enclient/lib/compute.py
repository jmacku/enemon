#!/usr/bin/env python
from twisted.internet import reactor, defer

import lib.config.defaults as defaults
from lib.varlist import VarList, VarItem
from lib.factory import Factory
from datetime import datetime, timezone, timedelta


class Filter:
    def __init__(self, filter_type=None):
        """
        Variable content - stored in one redis hash item
        xxx|yyy|z1|z2|z3|...

        xxx .. lenght of the filter (array)
        yyy .. current position for putting new value in
        z1 - zn .. filter individual items
        """
        self.filter_type = filter_type
        self.filter_items = []
        self.filter_len = 0
        self.filter_pos = 0

    def filter_unpack(self, packed_value):
        tmp_arr = [int(i) for i in packed_value.split('|')]
        if len(tmp_arr) > 2:
            self.filter_len = tmp_arr[0]
            self.filter_pos = tmp_arr[1]
            self.filter_items = tmp_arr[2:]

    def filter_pack(self):
        tmp_str = f'{self.filter_len}|{self.filter_pos}|'
        tmp_str = tmp_str + '|'.join([str(i) for i in self.filter_items])
        return tmp_str

    def filter_add_value(self, value):
        self.filter_items[self.filter_pos] = value
        self.filter_pos = (self.filter_pos + 1
                           if self.filter_pos < self.filter_len - 1
                           else 0)

    def get_avg_value(self):
        filter_sum = sum(self.filter_items)
        return filter_sum/self.filter_len


class RainGauge:
    def __init__(self, rain_per_mm):
        self.rain_per_mm = rain_per_mm

    def get_rain_mm(self, gauge_data):
        gauge_data_clean = gauge_data if (gauge_data[-1] != '|') else (
                gauge_data[:-1])
        gauge_data_arr = [
                val for val in gauge_data_clean.split('|') if int(val) > 0]
        rain_mm = len(gauge_data_arr) * self.rain_per_mm
        return rain_mm


class Compute:
    def __init__(self, factory):
        self.var_list = VarList()
        self.factory = factory

    def add_var(self, var_name, var_address,
                var_type, var_dir, var_def_value=0):
        self.var_list.add_var(
                var_name, var_address, var_type, var_dir, var_def_value)

    @defer.inlineCallbacks
    def read_var_list(self):
        values = yield self.factory.get_redis_helper().read_redis(
            [x.get_var_name() for x in
                self.var_list.iter_vars(
                    defaults.DIR_INPUT, defaults.ALL_TYPES_EXC_EDGE)]
        )
        for val, item in zip(
                values, (x for x in self.var_list.iter_vars(
                    defaults.DIR_INPUT, defaults.ALL_TYPES_EXC_EDGE))):
            item.set_var_value(val)
        values = yield self.factory.get_redis_helper().read_redis_edge(
            [x.get_var_name() for x in
                self.var_list.iter_vars(
                    defaults.DIR_INPUT, (defaults.TYPE_EDGE, ))]
        )
        for val, item in zip(
                values, (x for x in self.var_list.iter_vars(
                    defaults.DIR_INPUT, (defaults.TYPE_EDGE, )))):
            item.set_var_value(val)

    @defer.inlineCallbacks
    def write_var_list(self):
        yield self.factory.get_redis_helper().write_redis(
            [x.get_var_name() for x in self.var_list.iter_vars(
                defaults.DIR_OUTPUT)],
            [x.get_var_value() for x in self.var_list.iter_vars(
                defaults.DIR_OUTPUT)]
        )

    @defer.inlineCallbacks
    def check_var_list(self):
        yield self.factory.get_redis_helper().key_exist_redis(
            [x.get_var_name() for x in self.var_list.iter_vars(
                defaults.DIR_BIDIR, defaults.ALL_TYPES_EXC_EDGE)],
            [x.get_var_def_value() for x in self.var_list.iter_vars(
                defaults.DIR_BIDIR, defaults.ALL_TYPES_EXC_EDGE)]
        )

    def compute_calc(self):
        pass

    def compute_init(self):
        pass

    def cycle_start(self):
        pass

    def cycle_end(self):
        pass

    @defer.inlineCallbacks
    def compute_process(self):
        self.cycle_start()
        yield self.read_var_list()
        self.compute_calc()
        yield self.write_var_list()
        self.cycle_end()
        self.init_state = False

    @defer.inlineCallbacks
    def before_start(self):
        yield self.check_var_list()
        yield self.read_var_list()
        self.compute_init()
        yield self.write_var_list()
        self.init_state = False


class ComputeTemp(Compute):
    def __init__(self, factory):
        super().__init__(factory)
        self.add_var("TEMP_AVG", -1, defaults.TYPE_ANALOG, defaults.DIR_INPUT)
        self.add_var("TEMP", -1, defaults.TYPE_ANALOG, defaults.DIR_OUTPUT)

    def compute_calc(self):
        filter_read = self.var_list.get_by_name("TEMP_AVG").get_var_value()
        f_temp = Filter()
        f_temp.filter_upack(filter_read)
        temp_avg = f_temp.get_avg_value()
        temp_value = ((1.1/1024*temp_avg)-0.5)*100.0
        self.var_list.get_by_name("TEMP").set_var_value(temp_value)


class ComputeTempAvg(Compute):
    def __init__(self, factory):
        super().__init__(factory)
        self.add_var("TEMP_RAW", -1, defaults.TYPE_ANALOG, defaults.DIR_INPUT)
        self.add_var("TEMP_AVG", -1, defaults.TYPE_ANALOG,
                     defaults.DIR_BIDIR,
                     "30|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0"
                     "|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0")
        self.add_var("TEMP", -1, defaults.TYPE_ANALOG, defaults.DIR_OUTPUT)

    def compute_calc(self):
        temp_read = self.var_list.get_by_name("TEMP_RAW").get_var_value()
        f_temp = Filter()
        f_temp.filter_unpack(
                self.var_list.get_by_name("TEMP_AVG").get_var_value())
        f_temp.filter_add_value(temp_read)
        temp_avg = f_temp.get_avg_value()
        temp_value = ((1.1/1024*temp_avg)-0.5)*100.0
        f_pack = f_temp.filter_pack()
        self.var_list.get_by_name("TEMP_AVG").set_var_value(f_pack)
        self.var_list.get_by_name("TEMP").set_var_value(temp_value)


class ComputeInc(Compute):
    def __init__(self, factory):
        super().__init__(factory)
        self.add_var("COUNTER", -1, defaults.TYPE_ANALOG, defaults.DIR_BIDIR)
        self.counter = self.var_list.get_by_name("COUNTER")

    def compute_calc(self):
        num_read = self.counter.get_var_value()
        temp_value = num_read + 1
        self.counter.set_var_value(temp_value)

    def compute_init(self):
        self.counter.set_var_value(0)


class ComputeWatchdog(Compute):
    def __init__(self, factory):
        super().__init__(factory)
        self.add_var("WATCHDOG", -1, defaults.TYPE_DIGITAL, defaults.DIR_BIDIR)

    def compute_calc(self):
        my_var = self.var_list.get_by_name("WATCHDOG")
        cur_val = my_var.get_var_value()
        my_var.set_var_value(0 if cur_val else 1)


class ComputeTankTimer(Compute):
    def __init__(self, factory):
        super().__init__(factory)
        self.add_var("TANK_TIMER", -1, defaults.TYPE_DIGITAL,
                     defaults.DIR_BIDIR)
        self.tank_timer = self.var_list.get_by_name("TANK_TIMER")

    def compute_init(self):
        self.tank_timer.set_var_value(0)

    def compute_calc(self):
        cur_val = self.tank_timer.get_var_value()
        self.tank_timer.set_var_value(0 if cur_val else 1)


class ComputeTankStop(Compute):
    def __init__(self, factory):
        super().__init__(factory)
        self.add_var("TANK_TIMER", -1, defaults.TYPE_DIGITAL,
                     defaults.DIR_INPUT)
        self.add_var("TANK_STOP", -1, defaults.TYPE_DIGITAL,
                     defaults.DIR_OUTPUT)
        self.tank_timer = self.var_list.get_by_name("TANK_TIMER")
        self.tank_stop = self.var_list.get_by_name("TANK_STOP")

    def compute_calc(self):
        self.tank_stop.set_var_value(
                0 if self.tank_timer.get_var_value() else 1)


class ComputeTankLevel(Compute):
    def __init__(self, factory):
        super().__init__(factory)
        self.add_var("WL_LOW_PIN", -1, defaults.TYPE_DIGITAL,
                     defaults.DIR_INPUT)
        self.add_var("WL_MID_PIN", -1, defaults.TYPE_DIGITAL,
                     defaults.DIR_INPUT)
        self.add_var("WL_HIGH_PIN", -1, defaults.TYPE_DIGITAL,
                     defaults.DIR_INPUT)
        self.add_var("TANK_STATUS", -1, defaults.TYPE_ANALOG,
                     defaults.DIR_OUTPUT)
        self.low_pin = self.var_list.get_by_name("WL_LOW_PIN")
        self.mid_pin = self.var_list.get_by_name("WL_MID_PIN")
        self.high_pin = self.var_list.get_by_name("WL_HIGH_PIN")
        self.tank_status = self.var_list.get_by_name("TANK_STATUS")

    def compute_calc(self):
        level = defaults.TANK_LEVEL_EMPTY
        if self.low_pin.get_var_value():
            level += defaults.TANK_LEVEL_ALMOST_EMPTY
        if self.mid_pin.get_var_value():
            level += defaults.TANK_LEVEL_ALMOST_FULL
        if self.high_pin.get_var_value():
            level += defaults.TANK_LEVEL_FULL
        self.tank_status.set_var_value(level)


class ComputePumpAction(Compute):
    def __init__(self, factory):
        super().__init__(factory)
        self.add_var("AUTO_MODE", -1, defaults.TYPE_DIGITAL,
                     defaults.DIR_INPUT)
        self.add_var("NT", -1, defaults.TYPE_DIGITAL, defaults.DIR_INPUT)
        self.add_var("TANK_TIMER", -1, defaults.TYPE_DIGITAL,
                     defaults.DIR_INPUT)
        self.add_var("TANK_STATUS", -1, defaults.TYPE_ANALOG,
                     defaults.DIR_INPUT)
        self.add_var("TANK_TARGET", -1, defaults.TYPE_ANALOG,
                     defaults.DIR_INPUT)
        self.add_var("PUMP_REQ", -1, defaults.TYPE_DIGITAL, defaults.DIR_INPUT)
        self.add_var("PUMP_COMM", -1, defaults.TYPE_DIGITAL,
                     defaults.DIR_OUTPUT)
        self.auto_mode = self.var_list.get_by_name("AUTO_MODE")
        self.nt = self.var_list.get_by_name("NT")
        self.tank_timer = self.var_list.get_by_name("TANK_TIMER")
        self.tank_status = self.var_list.get_by_name("TANK_STATUS")
        self.tank_target = self.var_list.get_by_name("TANK_TARGET")
        self.pump_req = self.var_list.get_by_name("PUMP_REQ")
        self.pump_comm = self.var_list.get_by_name("PUMP_COMM")

    def compute_init(self):
        self.pump_comm.set_var_value(defaults.PUMP_OFF)
        self.auto_mode.set_var_value(defaults.AUTO_MODE_ON)

    def compute_calc(self):
        # safety pause
        if not self.tank_timer.get_var_value():
            self.pump_comm.set_var_value(defaults.PUMP_OFF)
            return
        # manual operation
        if not self.auto_mode.get_var_value():
            if (self.pump_req.get_var_value() and
                    self.tank_status.get_var_value() <
                    defaults.TANK_LEVEL_FULL):
                self.pump_comm.set_var_value(defaults.PUMP_ON)
            else:
                self.pump_comm.set_var_value(defaults.PUMP_OFF)
        # automatical operation
        else:
            self.pump_req.set_var_value(defaults.PUMP_OFF)
            if (self.nt.get_var_value() and
                    (self.tank_status.get_var_value() <
                     self.tank_target.get_var_value())):
                self.pump_comm.set_var_value(defaults.PUMP_ON)
            else:
                self.pump_comm.set_var_value(defaults.PUMP_OFF)


class ComputeRain(Compute):
    def __init__(self, factory, offset_hours=0, gauge_var_name='',
                 mm_var_name=''):
        super().__init__(factory)
        self.rain_pulse = self.get_rain_pulse_var(gauge_var_name)
        self.rain_mm = self.get_rain_mm_var(mm_var_name)
        self.rain_gauge = RainGauge(defaults.RAIN_MM_PER_PULSE)
        self.time_offset = offset_hours
        self.rain_pulse.set_suffix(self.get_suffix())

    def get_rain_pulse_var(self, gauge_var_name):
        self.add_var(
                gauge_var_name, -1, defaults.TYPE_EDGE, defaults.DIR_INPUT)
        return self.var_list.get_by_name(gauge_var_name)

    def get_rain_mm_var(self, mm_var_name):
        self.add_var(
                mm_var_name, -1, defaults.TYPE_ANALOG, defaults.DIR_OUTPUT)
        return self.var_list.get_by_name(mm_var_name)

    def get_suffix(self):
        dt = datetime.now(tz=timezone.utc)+timedelta(hours=self.time_offset)
        return dt.strftime('%Y%m%d%H')

    def compute_init(self):
        self.rain_mm.set_var_value(
                self.rain_gauge.get_rain_mm(self.rain_pulse.get_var_value()))

    def cycle_start(self):
        self.rain_pulse.set_suffix(self.get_suffix())

    def compute_calc(self):
        self.rain_mm.set_var_value(
                self.rain_gauge.get_rain_mm(self.rain_pulse.get_var_value()))
        self.rain_pulse.set_suffix(self.get_suffix())


# ===========================================================
if __name__ == '__main__':
    from twisted.internet import task
    # redis_connect(redis_helper)
    factory = Factory(defaults.REDIS_HOST, defaults.REDIS_PORT)
    compute_temp = ComputeTempAvg(factory)
    compute_inc = ComputeInc(factory)

    @defer.inlineCallbacks
    def prepare():
        global factory
        yield factory.factory_start()

    @defer.inlineCallbacks
    def cyclic_task():
        global compute_temp, compute_inc
        yield compute_temp.compute_process()
        yield compute_inc.compute_process()

    prepare()

    loop = task.LoopingCall(cyclic_task)
    loop.start(1.0, now=False)

    reactor.run()
