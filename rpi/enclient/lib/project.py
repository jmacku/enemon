#!/usr/bin/env python

from twisted.internet import reactor, defer
from lib.action_manager import Action, ActionList
from lib.config import defaults

from lib.io_module import RpIOEngine
from lib.compute import (
    ComputeTempAvg,
    ComputeInc,
    ComputeWatchdog,
    ComputeTankTimer,
    ComputeTankLevel,
    ComputePumpAction,
    ComputeTankStop,
    ComputeRain
)


class Action_001(Action):
    def __init__(self, factory):
        self.factory = factory
        self.action_id = 1
        self.period = defaults.IO_PERIOD
        self.rp_io = RpIOEngine(self.factory)
        super().__init__(self.action_id, self.factory.action_list, self.period)

    @defer.inlineCallbacks
    def action_job(self, *args, **kwargs):
        try:
            yield self.rp_io.io_loop()
        except Exception as Err:
            print(f'Action {self.action_id}: {Err}')

    @defer.inlineCallbacks
    def init_action(self):
        super().init_action()
        try:
            yield self.rp_io.init_io()
        except Exception as Err:
            print(f'Action init {self.action_id}: {Err}')

    @defer.inlineCallbacks
    def finish(self):
        yield self.rp_io.close_io()


class Action_002(Action):
    def __init__(self, factory):
        self.factory = factory
        self.action_id = 2
        self.period = defaults.IO_PERIOD
        self.compute_temp = ComputeTempAvg(self.factory)
        super().__init__(self.action_id, self.factory.action_list, self.period)

    @defer.inlineCallbacks
    def action_job(self, *args, **kwargs):
        try:
            yield self.compute_temp.compute_process()
        except Exception as Err:
            print(f'Action {self.action_id}: {Err}')

    @defer.inlineCallbacks
    def init_action(self):
        super().init_action()
        try:
            yield self.compute_temp.before_start()
        except Exception as Err:
            print(f'Action init {self.action_id}: {Err}')


class Action_003(Action):
    def __init__(self, factory):
        self.factory = factory
        self.action_id = 3
        self.period = defaults.COUNTER_PERIOD
        self.compute_inc = ComputeInc(self.factory)
        super().__init__(self.action_id, self.factory.action_list, self.period)

    @defer.inlineCallbacks
    def action_job(self, *args, **kwargs):
        try:
            yield self.compute_inc.compute_process()
        except Exception as Err:
            print(f'Action {self.action_id}: {Err}')

    @defer.inlineCallbacks
    def init_action(self):
        super().init_action()
        try:
            yield self.compute_inc.before_start()
        except Exception as Err:
            print(f'Action init {self.action_id}: {Err}')


class Action_004(Action):
    def __init__(self, factory):
        self.factory = factory
        self.action_id = 4
        self.period = defaults.WATCHDOG_PERIOD
        self.compute_watchdog = ComputeWatchdog(self.factory)
        super().__init__(self.action_id, self.factory.action_list, self.period)

    @defer.inlineCallbacks
    def action_job(self, *args, **kwargs):
        try:
            yield self.compute_watchdog.compute_process()
        except Exception as Err:
            print(f'Action {self.action_id}: {Err}')

    @defer.inlineCallbacks
    def init_action(self):
        super().init_action()
        try:
            yield self.compute_watchdog.before_start()
        except Exception as Err:
            print(f'Action init {self.action_id}: {Err}')


class Action_005(Action):
    def __init__(self, factory):
        self.factory = factory
        self.action_id = 5
        self.period = defaults.SAFE_PERIOD
        self.compute_tank_timer = ComputeTankTimer(self.factory)
        super().__init__(self.action_id, self.factory.action_list, self.period)

    @defer.inlineCallbacks
    def action_job(self, *args, **kwargs):
        try:
            yield self.compute_tank_timer.compute_process()
        except Exception as Err:
            print(f'Action {self.action_id}: {Err}')

    @defer.inlineCallbacks
    def init_action(self):
        super().init_action()
        try:
            yield self.compute_tank_timer.before_start()
        except Exception as Err:
            print(f'Action init {self.action_id}: {Err}')


class Action_006(Action):
    def __init__(self, factory):
        self.factory = factory
        self.action_id = 6
        self.period = defaults.TANK_PERIOD
        self.compute_tank_level = ComputeTankLevel(self.factory)
        super().__init__(self.action_id, self.factory.action_list, self.period)

    @defer.inlineCallbacks
    def action_job(self, *args, **kwargs):
        try:
            yield self.compute_tank_level.compute_process()
        except Exception as Err:
            print(f'Action {self.action_id}: {Err}')

    @defer.inlineCallbacks
    def init_action(self):
        super().init_action()
        try:
            yield self.compute_tank_level.before_start()
        except Exception as Err:
            print(f'Action init {self.action_id}: {Err}')


class Action_007(Action):
    def __init__(self, factory):
        self.factory = factory
        self.action_id = 7
        self.period = defaults.CONTROL_PERIOD
        self.compute_pump_action = ComputePumpAction(self.factory)
        super().__init__(self.action_id, self.factory.action_list, self.period)

    @defer.inlineCallbacks
    def action_job(self, *args, **kwargs):
        try:
            yield self.compute_pump_action.compute_process()
        except Exception as Err:
            print(f'Action {self.action_id}: {Err}')

    @defer.inlineCallbacks
    def init_action(self):
        super().init_action()
        try:
            yield self.compute_pump_action.before_start()
        except Exception as Err:
            print(f'Action init {self.action_id}: {Err}')


class Action_008(Action):
    def __init__(self, factory):
        self.factory = factory
        self.action_id = 8
        self.period = defaults.IO_PERIOD
        self.compute_tank_stop_action = ComputeTankStop(self.factory)
        super().__init__(self.action_id, self.factory.action_list, self.period)

    @defer.inlineCallbacks
    def action_job(self, *args, **kwargs):
        try:
            yield self.compute_tank_stop_action.compute_process()
        except Exception as Err:
            print(f'Action {self.action_id}: {Err}')

    @defer.inlineCallbacks
    def init_action(self):
        super().init_action()
        try:
            yield self.compute_tank_stop_action.before_start()
        except Exception as Err:
            print(f'Action init {self.action_id}: {Err}')


class Action_009(Action):
    def __init__(self, factory):
        self.factory = factory
        self.action_id = 9
        self.period = defaults.RAIN_PERIOD
        self.compute_rain_action = ComputeRain(
                self.factory, 0, 'RAIN_GAUGE_PULSE', 'RAIN_CURR_MM')
        super().__init__(self.action_id, self.factory.action_list, self.period)

    @defer.inlineCallbacks
    def action_job(self, *args, **kwargs):
        try:
            yield self.compute_rain_action.compute_process()
        except Exception as Err:
            print(f'Action {self.action_id}: {Err}')

    @defer.inlineCallbacks
    def init_action(self):
        super().init_action()
        try:
            yield self.compute_rain_action.before_start()
        except Exception as Err:
            print(f'Action init {self.action_id}: {Err}')


class Action_010(Action):
    def __init__(self, factory):
        self.factory = factory
        self.action_id = 10
        self.period = defaults.RAIN_PERIOD
        self.compute_rain_action = ComputeRain(
                self.factory, -1, 'RAIN_GAUGE_PULSE', 'RAIN_LAST_MM')
        super().__init__(self.action_id, self.factory.action_list, self.period)

    @defer.inlineCallbacks
    def action_job(self, *args, **kwargs):
        try:
            yield self.compute_rain_action.compute_process()
        except Exception as Err:
            print(f'Action {self.action_id}: {Err}')

    @defer.inlineCallbacks
    def init_action(self):
        super().init_action()
        try:
            yield self.compute_rain_action.before_start()
        except Exception as Err:
            print(f'Action init {self.action_id}: {Err}')
