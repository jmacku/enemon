#!/usr/bin/env python3

import pytest
import requests
import time

URL_GET = 'http://localhost:8080/api-get'
URL_SET = 'http://localhost:8080/api-set'

def api_call(mode, var_name, value=0):
    if mode == 'GET':
        url = f'{URL_GET}?name={var_name}'
        res = requests.get(url)
        retval = res.json()[0]
    elif mode == 'SET':
        url = f'{URL_SET}?{var_name}={value}'
        res = requests.get(url)
        retval = res.json()
    else:
        pass
    return(retval)


@pytest.mark.timeout(10)
def test_api_get():
    res = api_call('GET', 'COUNTER')
    assert int(res['COUNTER']) > 0

@pytest.mark.timeout(10)
def test_api_set():
    res = api_call('SET', 'TEMP', 25)
    assert res['cmd'] == 'ok'


if __name__ == '__main__':
    print(api_call('GET', 'COUNTER')['COUNTER'])
    print(api_call('SET', 'TEMP', 25))

