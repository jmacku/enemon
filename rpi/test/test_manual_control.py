#!/usr/bin/env python3

import pytest
import redis
import time
from os import environ
from ..enclient.lib.config import defaults
from ..enclient.lib.tools import check_rpi

redis_host = 'localhost' if 'REDIS_HOST' not in environ else environ['REDIS_HOST']
r = redis.Redis(
    host=redis_host,
    port=6379
)

skip_on_rpi = pytest.mark.skipif(
    check_rpi(), reason = "Cannot run on genuine Raspberry"
)

@pytest.fixture
def setup_tank():
    r.hset('ns1:AUTO_MODE', 'value', 0)
    r.hset('ns1:WL_MID_LOW', 'value', 1)
    r.hset('ns1:WL_MID_PIN', 'value', 1)
    r.hset('ns1:WL_HIGH_PIN', 'value', 0)
    r.hset('ns1:PUMP_REQ', 'value', 1)
    return True

@pytest.mark.timeout(190)
def test_pump_on(setup_tank):
    tmp = setup_tank
    time.sleep(defaults.TANK_PERIOD + 1)
    print('Waiting for tank to stabilize')
    while int(r.hget('ns1:TANK_TIMER', 'value')) == 0:
        time.sleep(1)
    print('Waiting for control to stabilize')
    time.sleep(defaults.CONTROL_PERIOD + 1)
    pump_comm = r.hget('ns1:PUMP_COMM', 'value')
    assert int(pump_comm) == 1

@pytest.mark.timeout(190)
def test_pump_off():
    r.hset('ns1:PUMP_REQ', 'value', 0)
    time.sleep(defaults.TANK_PERIOD + 1)
    while int(r.hget('ns1:TANK_TIMER', 'value')) == 0:
        time.sleep(1)
    time.sleep(defaults.CONTROL_PERIOD + 1)
    pump_comm = r.hget('ns1:PUMP_COMM', 'value')
    assert int(pump_comm) == 0

@skip_on_rpi
@pytest.mark.timeout(190)
def test_pump_off_tank_full():
    r.hset('ns1:PUMP_REQ', 'value', 1)
    r.hset('ns1:WL_HIGH_PIN', 'value', 1)
    time.sleep(defaults.TANK_PERIOD + 1)
    while int(r.hget('ns1:TANK_TIMER', 'value')) == 0:
        time.sleep(1)
    time.sleep(defaults.CONTROL_PERIOD + 1)
    pump_comm = r.hget('ns1:PUMP_COMM', 'value')
    assert int(pump_comm) == 0



