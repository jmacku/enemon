#!/usr/bin/env python3

import pytest
import redis
import time
from os import environ

from ..enclient.lib.tools import check_rpi

redis_host = 'localhost' if 'REDIS_HOST' not in environ else environ['REDIS_HOST']
r = redis.Redis(
    host = redis_host,
    port = 6379
)

skip_on_rpi = pytest.mark.skipif(
    check_rpi(), reason = "Cannot run on genuine Raspberry"
)


@pytest.fixture
def empty_tank():
    r.hset('ns1:WL_LOW_PIN', 'value', 0)
    r.hset('ns1:WL_MID_PIN', 'value', 0)
    r.hset('ns1:WL_HIGH_PIN', 'value', 0)
    return True

@skip_on_rpi
def test_tank_low_level(empty_tank):
    tmp = empty_tank
    r.hset('ns1:WL_LOW_PIN', 'value', 1)
    time.sleep(6)
    tank_level = r.hget('ns1:TANK_STATUS', 'value')
    assert int(tank_level) == 1

@skip_on_rpi
def test_tank_mid_level():
    r.hset('ns1:WL_MID_PIN', 'value', 1)
    time.sleep(6)
    tank_level = r.hget('ns1:TANK_STATUS', 'value')
    assert int(tank_level) >= 2

@skip_on_rpi
def test_tank_high_level():
    r.hset('ns1:WL_HIGH_PIN', 'value', 1)
    time.sleep(6)
    tank_level = r.hget('ns1:TANK_STATUS', 'value')
    assert int(tank_level) >= 4

def test_tank_stop():
    tank_timer = r.hget('ns1:TANK_TIMER', 'value')
    tank_stop = r.hget('ns1:TANK_STOP', 'value')
    assert int(tank_timer) != int(tank_stop)
