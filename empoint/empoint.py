#!/usr/bin/env python

import curses
import datetime
import calendar
import os
from time import sleep
from pytz import utc
import time
import threading
from enemonsql import cEnemonSQL
from redis_sapi import RedisHelper
import argparse

from emplogger import Log
from point import cListOfPoints
from protocol import cModbusProtocol, cWebProtocol
from datehelper import cMyDate
from point import cListOfPoints, cVisualPoint, cTMPPoint, cNTCPoint  # noqa F401
from read_config import cConfig
import constants

_VER_ = "0.30"

MODE_MANUAL = 0
MODE_AUTOMATIC = 1

AUNO_SLAVE_ID = 1
ANANO_SLAVE_ID = 2

TIME_TOL = 5         # .. time tolerance in sec

ARC_IDLE = 0
ARC_REQ = 1
ARC_RES = 2
ARC_ERROR = -1

MIN_RAIN_FC = 3      # -- minimal rain forecast to lover the tank target level

pid_file = 'empoint.pid'


def fnWritePid():
    my_pid = os.getpid()
    with open(pid_file, 'w') as f:
        f.write(str(my_pid))


def fnDeletePid():
    try:
        os.remove(pid_file)
    except Exception:
        pass


class cWindow:
    """ """
    def __init__(self, winObj, sHeader, redis_helper=None):
        """ """
        self.oWin = winObj
        self.sHeader = sHeader
        self.aListOfPoints = cListOfPoints(redis_helper)
        self.aPoints = {}

    def fnAddPoint(self, iLX, iLY, sLabel, iPX, iPY, oP):
        """ """
        if oP is None:
            return
        n = oP.sfnGetName()
        d = {}
        d['LX'] = iLX
        d['LY'] = iLY
        d['LABEL'] = sLabel
        d['PX'] = iPX
        d['PY'] = iPY
        d['POINT'] = oP
        self.aPoints[n] = d
        self.aListOfPoints.addPointObject(oP)

    def fnPrepare(self):
        """ """
        self.oWin.border(0)
        if self.sHeader != "":
            (dr, dc) = self.oWin.getmaxyx()
            if len(self.sHeader) < dc:
                self.oWin.addstr(0, (dc-len(self.sHeader))//2, self.sHeader,
                                 curses.A_REVERSE)
        for k, v in self.aPoints.items():
            self.oWin.addstr(v['LY'], v['LX'], v['LABEL'])
        self.oWin.refresh()

    def fnShow(self):
        """ """
        self.aListOfPoints.read_point_list(constants.DIR_ALL)
        for k, v in self.aPoints.items():
            v['POINT'].fnShowPoint(v['PX'], v['PY'], self.oWin)
        self.oWin.refresh()

    def fnPut(self, iX, iY, sS, attr=curses.A_NORMAL):
        """ """
        self.oWin.addstr(iY, iX, sS, attr)


class cAWindow(cWindow):
    """ """
    def __init__(self, winObj, sHeader, oArch, redis_helper=None):
        cWindow.__init__(self, winObj, sHeader, redis_helper)
        self.oArch = oArch

    def fnShowAPoint(self, dt, AValue, iRow):
        """ """
        self.oWin.addstr(iRow, 1, dt.strftime("%d.%m.%Y %H:%M"))
        self.oWin.addstr(iRow, 18, "N:%1.1f" % AValue['f_energy_lt'])
        self.oWin.addstr(iRow, 24, "V:%1.1f" % AValue['f_energy_ht'])

    def fnShowArch(self):
        """ """
        iPos = 1
        for k in sorted(self.oArch):
            self.fnShowAPoint(k, self.oArch[k], iPos)
            iPos += 1

    def fnShow(self):
        """ """
        cWindow.fnShow(self)
        self.fnShowArch()


class cArchive:
    """ """
    def __init__(self, iArchLen):
        """ """
        self.iArchLen = iArchLen
        self.aArch = {}

    def fnAddToArch(self, dt, fEnLT, fEnHT, fTout=0, fTin=0):
        """ """
        d = {}
        d['f_energy_lt'] = fEnLT
        d['f_energy_ht'] = fEnHT
        d['f_tout_avg'] = fTout
        d['f_tin_avg'] = fTin
        self.aArch[dt] = d
        if len(self.aArch) > self.iArchLen:
            oldest = min(self.aArch, key=self.aArch.get)
            self.aArch.pop(oldest, None)

    def fnIsInArch(self, dt):
        """ """
        if dt in self.aArch:
            return False
        else:
            return True


class cEneMon:
    """ """
    def __init__(self, sDevice, sConnStr):
        """ """
        self.boHalt = False
        self.sVer = _VER_
        self.oScreen = curses.initscr()
        self.oArchive = cArchive(100)
        self.oSQL = cEnemonSQL(sConnStr)
        self.redis_helper = RedisHelper('localhost')
        self.redis_helper.connect_redis()
        self.oCfg = cConfig('config.yml')
        self.oCfg.load_config()
        self.oLoP = cListOfPoints(redis_helper=self.redis_helper)
        self.oCfg.add_points_to_list(self.oLoP)
        self.iLastStoredMinute = -1
        self.iLastStoredHour = -1
        self.iLastCommStat = -1
        self.iLastCommStatSwap = -1
        self.iLastRainForecast = -1
        self.arcStatus = ARC_IDLE
        self.eTime = 0
        curses.start_color()
        curses.noecho()
        curses.curs_set(0)
        self.oScreen.keypad(1)
        # -- self.oScreen.border(0)
        self.oScreen.addstr(0, 0,
                            "  Arduino Energy Monitor v %s" % (self.sVer),
                            curses.A_REVERSE)
        self.oScreen.chgat(-1, curses.A_REVERSE)
        self.oScreen.addstr(curses.LINES-1, 3, "q", curses.A_BOLD)
        self.oScreen.addstr(curses.LINES-1, 4, "uit")
        self.oScreen.addstr(curses.LINES-1, 8, "a", curses.A_BOLD)
        self.oScreen.addstr(curses.LINES-1, 9, "rchive")
        self.oActWin = self.oScreen.subwin(9, 28, 2, 1)
        self.oTimWin = self.oScreen.subwin(12, 28, 11, 1)
        # self.oTimWin=self.oScreen.subwin(9, 27, 2, 35)
        self.oArcWin = self.oScreen.subwin(12, 50, 2, 30)
        self.oTnkWin = self.oScreen.subwin(9,  16, 14, 48)
        self.oTemWin = self.oScreen.subwin(9,  16, 14, 30)
        self.oGasWin = self.oScreen.subwin(4,  15, 14, 65)
        self.oRainWin = self.oScreen.subwin(5,  15, 18, 65)
        curses.init_pair(1, curses.COLOR_WHITE, curses.COLOR_BLACK)
        curses.init_pair(2, curses.COLOR_GREEN, curses.COLOR_BLACK)
        curses.init_pair(3, curses.COLOR_RED, curses.COLOR_BLACK)

        self.oMyDate = cMyDate()
        self.dtStartTime = self.oMyDate.fnGetUTCTime()

        self.oMP = cModbusProtocol(sDevice, redis_helper=self.redis_helper)
        self.oMP.createCommNode(AUNO_SLAVE_ID)
        self.oMP.createCommNode(ANANO_SLAVE_ID)
        for point in self.oLoP.iter_points_prot('modbus'):
            self.oMP.addPointObject(point)
        self.oWP = cWebProtocol(redis_helper=self.redis_helper)
        for point in self.oLoP.iter_points_prot('web'):
            self.oWP.addPointObject(point)

        self.oAW = cWindow(self.oActWin, "Current values", self.redis_helper)
        self.oAW.fnAddPoint(1, 1, "Tarif:",
                            8,  1, self.oMP.fnGetPointByName("TARIF"))
        self.oAW.fnAddPoint(1, 2, "Power:",
                            19,  2, self.oMP.fnGetPointByName("POWER"))
        self.oAW.fnAddPoint(1, 3, "Curr Energy NT:",
                            19,  3, self.oMP.fnGetPointByName("CURR_EN_LT"))
        self.oAW.fnAddPoint(1, 4, "Curr Energy VT:",
                            19,  4, self.oMP.fnGetPointByName("CURR_EN_HT"))
        self.oAW.fnAddPoint(1, 5, "Last Energy NT:",
                            19,  5, self.oMP.fnGetPointByName("LAST_EN_LT"))
        self.oAW.fnAddPoint(1, 6, "Last Energy VT:",
                            19,  6, self.oMP.fnGetPointByName("LAST_EN_HT"))
        self.oAW.fnAddPoint(1, 7, "ArTime:",
                            8,  7, self.oMP.fnGetPointByName("ARD_TIME"))
        self.oAW.fnPrepare()

        self.oTW = cWindow(self.oTimWin, "Times", self.redis_helper)
        self.oTW.fnAddPoint(1, 5,  "SyT:", 6, 5,
                            self.oMP.fnGetPointByName("SYNC_TIME"))
        self.oTW.fnAddPoint(1, 6,  "SyF:", 6, 6,
                            self.oMP.fnGetPointByName("SYNC_FLAG"))
        self.oTW.fnAddPoint(1, 7,  "RqT:", 6, 7,
                            self.oMP.fnGetPointByName("RQ_HIST_TIME"))
        self.oTW.fnAddPoint(1, 8,  "RsT:", 6, 8,
                            self.oMP.fnGetPointByName("RS_HIST_TIME"))
        self.oTW.fnAddPoint(1, 9,  "LT:",  6, 9,
                            self.oMP.fnGetPointByName("RS_HIST_LT_ENER"))
        self.oTW.fnAddPoint(1, 10, "HT:",  6, 10,
                            self.oMP.fnGetPointByName("RS_HIST_HT_ENER"))
        self.oTW.fnPrepare()

        self.oHW = cAWindow(self.oArcWin, "Archive", self.oArchive.aArch,
                            self.redis_helper)
        (dy, dx) = self.oHW.oWin.getmaxyx()
        self.oHW.fnAddPoint(2,     dy-1, "Size:    ",    7, dy-1,
                            self.oMP.fnGetPointByName("ARCH_DEPTH"))
        self.oHW.fnAddPoint(dx-11, dy-1, "Read:    ", dx-6, dy-1,
                            self.oMP.fnGetPointByName("ARCH_READ"))
        self.oHW.fnPrepare()

        self.oGW = cWindow(self.oGasWin, "Gas", self.redis_helper)
        self.oGW.fnAddPoint(2, 1, "Act:", 5, 1,
                            self.oMP.fnGetPointByName("GAS_BOILER_STATUS"))
        self.oGW.fnAddPoint(2, 2, "BTO:", 5, 2,
                            self.oMP.fnGetPointByName("GAS_BOILER_TIME"))
        self.oGW.fnPrepare()

        self.oRW = cWindow(self.oRainWin, "Rain", self.redis_helper)
        self.oRW.fnAddPoint(2, 1, "Act:", 5, 1,
                            self.oWP.fnGetPointByName("RAIN_CURR_MM"))
        self.oRW.fnAddPoint(2, 2, "Lst:", 5, 2,
                            self.oWP.fnGetPointByName("RAIN_LAST_MM"))
        self.oRW.fnPrepare()

        self.oBW = cWindow(self.oTnkWin, "Tank", self.redis_helper)
        self.oBW.fnAddPoint(2, 1, "Rfc:", 7, 1,
                            self.oWP.fnGetPointByName("RAIN_FC"))
        self.oBW.fnAddPoint(2, 2, "Tlv:",  7, 2,
                            self.oWP.fnGetPointByName("TANK"))
        self.oBW.fnAddPoint(2, 3, "Pmp:",  7, 3,
                            self.oWP.fnGetPointByName("PUMP"))
        self.oBW.fnAddPoint(2, 4, "Mod:",  7, 4,
                            self.oWP.fnGetPointByName("MODE"))
        self.oBW.fnAddPoint(2, 5, "Tlc:",  7, 5,
                            self.oWP.fnGetPointByName("TARGET_LEVEL"))
        self.oBW.fnAddPoint(2, 6, "Tlr:",  7, 6,
                            self.oWP.fnGetPointByName("CURR_T_LEVEL"))
        self.oBW.fnPrepare()

        self.oEW = cWindow(self.oTemWin, "Temp", self.redis_helper)
        self.oEW.fnAddPoint(2, 1, "Ext:",  8, 1,
                            self.oWP.fnGetPointByName("TEMP_OUT"))
        self.oEW.fnAddPoint(2, 2, "Int:",  8, 2,
                            self.oWP.fnGetPointByName("TEMP_INT"))
        self.oEW.fnAddPoint(2, 3, "Gar:",  8, 3,
                            self.oWP.fnGetPointByName("TEMP_GARAGE"))
        self.oEW.fnAddPoint(2, 4, "Cel:",  8, 4,
                            self.oMP.fnGetPointByName("TEMP_BM"))
        self.oEW.fnAddPoint(2, 5, "Hw1:",  8, 5,
                            self.oMP.fnGetPointByName("TEMP_HW1"))
        self.oEW.fnAddPoint(2, 6, "Hw2:",  8, 6,
                            self.oMP.fnGetPointByName("TEMP_HW2"))
        self.oEW.fnPrepare()

    def fnShowUTCTime(self, oWin):
        """ """
        oWin.fnPut(1, 1,
                   'UTC: ' + self.oMyDate.fnGetUTCTime().strftime(
                       "%d.%m.%Y %H:%M:%S"))

    def fnShowLocTime(self, oWin):
        """ """
        oWin.fnPut(1, 2,
                   'LOC: ' + self.oMyDate.fnGetLocTime().strftime(
                       "%d.%m.%Y %H:%M:%S"))

    def fnGetArdTime(self):
        """ """
        p = self.oMP.fnGetPointByName("ARD_TIME")
        if p is not None:
            dt = datetime.datetime.fromtimestamp(int(p.Value), utc)
        else:
            dt = 0
        return dt

    def fnShowArdTime(self, oWin):
        """ """
        oWin.fnPut(1, 3,
                   'Ard: ' + self.fnGetArdTime().strftime("%d.%m.%Y %H:%M:%S"))

    def fnSyncTime(self):
        """ """
        dtArdo = self.fnGetArdTime()
        dtHost = self.oMyDate.fnGetUTCTime()
        ldtArdo = calendar.timegm(dtArdo.utctimetuple())
        ldtHost = calendar.timegm(dtHost.utctimetuple())
        Log.debug("Ard time: %s" % ldtArdo)
        Log.debug("Hst time: %s" % ldtHost)
        pSF = self.oMP.fnGetPointByName("SYNC_FLAG")
        if pSF is None:
            return
        pST = self.oMP.fnGetPointByName("SYNC_TIME")
        if pST is None:
            return
        if (abs(ldtArdo-ldtHost) > TIME_TOL):
            Log.debug("Activating the time synchronization...")
            pSF.fnSetValue(1)
            pSF.fnSetSyncReq(True)
            pST.fnSetValue(ldtHost)
            pST.fnSetSyncReq(True)
            self.boTSync = True
        else:
            if self.boTSync:
                pSF.fnSetValue(0)
                pSF.fnSetSyncReq(True)
                self.boTSync = False
            else:
                pSF.fnSetSyncReq(False)
        points = [pSF.sfnGetName(), pST.sfnGetName()]
        values = [pSF.gfnGetRawValue(), pST.gfnGetRawValue()]
        self.redis_helper.write_redis(points, values)

    def fnReqHistValue(self, dt):
        """ """
        if not self.oMP.boConnection:
            return
        pRQdt = self.oMP.fnGetPointByName("RQ_HIST_TIME")
        pRSdt = self.oMP.fnGetPointByName("RS_HIST_TIME")
        if pRQdt:
            pRQdt.fnSetValue(dt)
            pRQdt.fnSetSyncReq(True)
            pRSdt.fnSetSyncReq(True)

    def bofnIsRes(self):
        """ """
        pRQdt = datetime.datetime.fromtimestamp(
                self.oMP.fnGetPointByName("RQ_HIST_TIME").gfnGetValue(), utc)
        pRSdt = datetime.datetime.fromtimestamp(
                self.oMP.fnGetPointByName("RS_HIST_TIME").gfnGetValue(), utc)
        if pRQdt == pRSdt:
            return (True)
        else:
            return (False)

    def fnPopArchPoint(self):
        """ """
        pRSdt = datetime.datetime.fromtimestamp(
                self.oMP.fnGetPointByName("RS_HIST_TIME").gfnGetValue(), utc)
        self.oArchive.fnAddToArch(
                pRSdt,
                self.oMP.fnGetPointByName("RS_HIST_LT_ENER").gfnGetValue(),
                self.oMP.fnGetPointByName("RS_HIST_HT_ENER").gfnGetValue())

    def fnGetHistValue(self, dt):
        """ """
        iAtempt = 0
        if self.arcStatus != ARC_IDLE:
            return
        self.arcStatus = ARC_REQ
        self.fnReqHistValue(dt)
        sleep(1.0)
        while True:
            if self.arcStatus == ARC_REQ:
                self.oMP.fnGetPointByName("RS_HIST_TIME").fnSetSyncReq(True)
                if self.bofnIsRes():
                    self.oMP.fnGetPointByName("RS_HIST_LT_ENER").fnSetSyncReq(
                            True)
                    self.oMP.fnGetPointByName("RS_HIST_HT_ENER").fnSetSyncReq(
                            True)
                    self.arcStatus = ARC_RES
                else:
                    iAtempt += 1
                    if iAtempt > 9:
                        self.arcStatus = ARC_ERROR
                        break
            elif self.arcStatus == ARC_RES:
                self.fnPopArchPoint()
                self.arcStatus = ARC_IDLE
                break
            else:
                break
            sleep(1.0)

    def fnGetHistValues(self):
        """ """
        dtArcStart = self.oMyDate.fnGetLastHour()
        dtArcEnd = dtArcStart + datetime.timedelta(0, -60*60*10)
        while self.oMP.fnGetPointByName("ARCH_READ").gfnGetValue() > 0:
            self.arcStatus = ARC_IDLE
            self.fnGetHistValue(calendar.timegm(dtArcStart.utctimetuple()))
            if dtArcStart > dtArcEnd:
                dtArcStart = dtArcStart + datetime.timedelta(0, -60*60)
            else:
                self.oMP.fnGetPointByName("ARCH_READ").fnSetValue(0)
                break

    def fnGetHistValuesT(self):
        """ """
        while True:
            if int(self.oMP.fnGetPointByName("ARCH_READ").gfnGetValue()) > 0:
                self.fnGetHistValues()
            if self.boHalt:
                break
            sleep(0.5)

    def fnToggleArchRead(self):
        """ """
        pAR = self.oMP.fnGetPointByName("ARCH_READ")
        pV = pAR.gfnGetValue()
        pAR.fnSetValid()
        if pV == 0:
            pAR.fnSetValue(1)
        else:
            pAR.fnSetValue(0)

    def fnShowError(self, oWin):
        """ """
        oWin.addstr(1, 10, str(self.oWP.sError)[:60],
                    curses.color_pair(3) | curses.A_BOLD)

    def fnStorePower(self):
        """ """
        dt = self.oMyDate.fnGetUTCTime()
        if (dt.second > 15) and (dt.minute != self.iLastStoredMinute):
            self.iLastStoredMinute = dt.minute
            dt = dt + datetime.timedelta(0, (dt.second)*(-1), -dt.microsecond)
            self.oSQL.fnStoreDetail(
                dt,
                int(self.oMP.fnGetPointByName("TARIF").gfnGetValue()),
                int(self.oMP.fnGetPointByName("POWER").gfnGetValue()),
                int(self.oMP.fnGetPointByName("SYNC_FLAG").gfnGetValue()),
                float(self.oWP.fnGetPointByName("TEMP_OUT").gfnGetValue()),
                float(self.oWP.fnGetPointByName("TEMP_INT").gfnGetValue()),
                int(self.oMP.fnGetPointByName(
                    "GAS_BOILER_STATUS").gfnGetValue()),
                float(self.oWP.fnGetPointByName("RAIN_CURR_MM").gfnGetValue()))
            self.oSQL.fnStoreTank(
                dt,
                float(self.oWP.fnGetPointByName("TANK").gfnGetValue()),
                float(self.oWP.fnGetPointByName("PUMP").gfnGetValue()),
                float(self.oWP.fnGetPointByName("MODE").gfnGetValue()))
            self.oSQL.fnStoreTemp(
                dt,
                float(self.oWP.fnGetPointByName(
                    "TEMP_GARAGE").gfnGetValue()),
                float(self.oMP.fnGetPointByName("TEMP_BM").gfnGetValue()),
                float(self.oMP.fnGetPointByName("TEMP_HW1").gfnGetValue()),
                float(self.oMP.fnGetPointByName("TEMP_HW2").gfnGetValue()))

    def fnStoreEnergy(self):
        """ """
        dt = self.oMyDate.fnGetUTCTime()
        if (dt.minute == 2) and (dt.hour != self.iLastStoredHour):
            self.iLastStoredHour = dt.hour
            dt = dt + datetime.timedelta(0, (dt.minute*60+dt.second)*(-1),
                                         -dt.microsecond)
            self.oSQL.fnStoreEnergy(
                dt,
                float(self.oMP.fnGetPointByName("LAST_EN_LT").gfnGetValue()),
                float(self.oMP.fnGetPointByName("LAST_EN_HT").gfnGetValue()),
                0, 0,
                int(self.oMP.fnGetPointByName(
                    "GAS_BOILER_TIME").gfnGetValue()))
            self.oSQL.fnStoreRain(
                dt,
                float(self.oWP.fnGetPointByName("RAIN_LAST_MM").gfnGetValue()))

    def fnCommStat(self):
        """ """
        dt = self.oMyDate.fnGetUTCTime()
        if (dt.minute == 0) and (dt.hour != self.iLastCommStatSwap):
            self.iLastCommStatSwap = dt.hour
            self.oMP.fnSwapErrCount()

    def fnStoreCommStat(self):
        """ """
        dt = self.oMyDate.fnGetUTCTime()
        if (dt.minute == 1) and (dt.hour != self.iLastCommStat):
            self.iLastCommStat = dt.hour
            dt = dt + datetime.timedelta(0,
                                         (dt.minute*60+dt.second)*(-1),
                                         -dt.microsecond)
            self.oSQL.fnStoreCommStat(dt,
                                      self.oMP.nMsgOKlst,
                                      self.oMP.nMsgKO_IOlst,
                                      self.oMP.nMsgKO_Vallst,
                                      self.oMP.nMsgKO_Typlst)

    def fnReadRainForecast(self):
        """ """
        dt = self.oMyDate.fnGetUTCTime()
        if (dt.minute % 3 == 0) and (dt.minute != self.iLastRainForecast):
            self.iLastRainForecast = dt.minute
            rain = self.oSQL.ifnGetRainForecast()
            oP = self.oWP.fnGetPointByName("RAIN_FC")
            oP.fnSetValue(rain)
            oTL = self.oWP.fnGetPointByName("TARGET_LEVEL")
            if rain < 0:
                oP.fnSetInvalid()
                oTL.fnSetInvalid()
            else:                                 # -- valid forecast value
                oP.fnSetValid()
                oTL.fnSetValid()
                if rain >= MIN_RAIN_FC:           # -- check minimal rain value
                    oTL.fnSetValue(constants.TARGET_LEVEL_ALMOST_FULL)
                else:
                    oTL.fnSetValue(constants.TARGET_LEVEL_FULL)
                oTL.fnSetSyncReq(True)            # -- sync value with slave
            points = [oP.sfnGetName(), oTL.sfnGetName()]
            values = [oP.gfnGetValue(), oTL.gfnGetValue()]
            self.redis_helper.write_redis(points, values)

    def fnUpdateDatabase(self):
        """ """
        while True:
            iTb = int(round(time.time() * 1000))
            self.fnStorePower()
            self.fnStoreEnergy()
            self.fnStoreCommStat()
            self.fnReadRainForecast()
            if self.boHalt:
                break
            iTe = int(round(time.time() * 1000))
            sleepTime = 1.0-(iTe-iTb)/1000.0
            if sleepTime > 0:
                sleep(sleepTime)

    def fnCheckKeyPressed(self):
        """ """
        while True:
            # self.oScreen.nodelay(1)
            event = self.oScreen.getch()
            if event == ord("q"):
                self.boHalt = True
                break
            if event == ord('a'):
                self.fnToggleArchRead()

    def fnShowUpTime(self, oWin):
        """ """
        td = (self.oMyDate.fnGetUTCTime() - self.dtStartTime)
        iS = (td.microseconds + (td.seconds + td.days * 24 * 3600)
              * 10**6) / 10**6
        iH, iR = divmod(iS, 3600)
        iM, iS = divmod(iR, 60)
        iD, iH = divmod(iH, 24)
        oWin.addstr(0, 67, "%3d %02d:%02d:%02d" % (iD, iH, iM, iS),
                    curses.A_REVERSE)

    def fnRun(self):
        """ """
        while not self.boHalt:
            iTb = int(round(time.time() * 1000))
            self.fnShowUpTime(self.oScreen)
            self.fnCommStat()
            self.fnSyncTime()
            self.oMP.runPoints()
            self.oAW.fnShow()
            self.fnShowError(self.oScreen)
            self.fnShowUTCTime(self.oTW)
            self.fnShowLocTime(self.oTW)
            self.fnShowArdTime(self.oTW)
            self.oTW.fnShow()
            self.oHW.fnShow()
            self.oEW.fnShow()
            self.oBW.fnShow()
            self.oGW.fnShow()
            self.oRW.fnShow()
            self.oScreen.refresh()
            iTe = int(round(time.time() * 1000))
            sleepTime = 1.0-(iTe-iTb)/1000.0
            if sleepTime > 0:
                sleep(sleepTime)

    def fnWebComm(self):
        """ """
        while not self.boHalt:
            iTb = int(round(time.time() * 1000))
            self.oWP.runPoints()
            iTe = int(round(time.time() * 1000))
            sleepTime = 10.0-(iTe-iTb)/1000.0
            if sleepTime > 0:
                sleep(sleepTime)

    def fnDone(self):
        """ """
        curses.endwin()
        self.redis_helper.disconnect_redis()


def cmd_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("--cs", required=True, help="Connection string")
    parser.add_argument("--dev", default="/dev/ttyUSB0", help="Serial device")
    return parser


# -------------------------------------------------------------------------------
if __name__ == '__main__':

    fnWritePid()
    Log.setTextLogging(level=Log.INFO)

    args = cmd_parser().parse_args()
    Log.info('Application start')
    oEneMon = cEneMon(args.dev, args.cs)

    thRun = threading.Thread(name='modbus',   target=oEneMon.fnRun)
    thWeb = threading.Thread(name='web',      target=oEneMon.fnWebComm)
    thArc = threading.Thread(name='archiver', target=oEneMon.fnGetHistValuesT)
    thSQLP = threading.Thread(name='database', target=oEneMon.fnUpdateDatabase)

    thRun.start()
    thWeb.start()
    thArc.start()
    thSQLP.start()

    oEneMon.fnCheckKeyPressed()
    thRun.join()
    thWeb.join()
    thArc.join()
    thSQLP.join()

    oEneMon.fnDone()
    Log.info('Application stop')
    fnDeletePid()
