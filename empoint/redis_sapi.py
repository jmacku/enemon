from redis import StrictRedis as Redis

_DEFAULT_NS = 'ns1'


class KeyName(object):
    """
    """
    def __init__(self, ns=_DEFAULT_NS):
        self.ns = ns

    def add_ns(self, name):
        nsname = self.ns + ':' + name if self.ns else name
        return nsname

    def strip_ns(self, name):
        sname = name.split(':')[0] if self.ns else name
        return sname


class RedisHelper(object):
    """
    """
    def __init__(self, nodeid, port=6379, dbid=0):
        self.nodeid = nodeid
        self.port = port
        self.dbid = dbid
        self.connected = False
        self.redis_handler = None
        self.key_name = KeyName()

    def connect_redis(self):
        """
        """
        try:
            self.redis_handler = Redis(
                    self.nodeid,
                    self.port,
                    self.dbid)
            self.connected = True
        except Exception as error:
            print(error)

    def disconnect_redis(self):
        try:
            self.redis_handler.connection_pool.disconnect()
        except Exception as error:
            print(error)

    def write_redis(self, names, vals):
        """
        """
        if len(names) != len(vals):
            print("The length of names and values does NOT match!")
            return
        try:
            pipe = self.redis_handler.pipeline()
            for name, val in zip(names, vals):
                nsname = self.key_name.add_ns(name)
                pipe.hset(nsname, "value", val)
            pipe.execute()
        except Exception as error:
            print(error)

    def read_redis(self, names):
        """
        """
        if not len(names):
            print("Nothing to do, empty list!")
            return
        try:
            pipe = self.redis_handler.pipeline()
            for name in names:
                nsname = self.key_name.add_ns(name)
                pipe.hget(nsname, "value")
            return pipe.execute()
        except Exception as error:
            print(error)
            return None


# =============================================================================
if __name__ == '__main__':
    my_redis = RedisHelper('localhost')
    my_redis.connect_redis()
    my_redis.write_redis(['a0', 'a1', 'a2'], [11, 22, 33])
    vals = my_redis.read_redis(['a0', 'a1', 'a2'])
    print(vals)
    my_redis.disconnect_redis()
