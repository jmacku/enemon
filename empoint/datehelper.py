#!/usr/bin/env python

import datetime
from pytz import timezone, utc


class cMyDate:
    """ """
    def __init__(self):
        """ """
        self.utc = utc
        self.local = timezone("Europe/Prague")

    def fnGetUTCTime(self):
        """ """
        self.dt = datetime.datetime.now(self.utc)
        return (self.dt)

    def fnGetLocTime(self):
        """ """
        self.dt = datetime.datetime.now(self.utc)
        ltime = self.dt.astimezone(self.local)
        return (ltime)

    def fnGetLastHour(self):
        """ """
        mydt = self.fnGetUTCTime()
        mydt = mydt + datetime.timedelta(0, (mydt.minute*60+mydt.second)*(-1),
                                         -mydt.microsecond)
        return (mydt)
