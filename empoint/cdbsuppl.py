#!/usr/bin/python
# -------------------------------------------------------------------------/
#                                                                          /
#                           cdbsuppl.py                                    /
#             Module for generic database access support                   /
#                                                                          /
# -------------------------------------------------------------------------/
#                                                                          /
# -------------------------------------------------------------------------/
#  This file implements the SQLite database behavior                       /
# -------------------------------------------------------------------------/
import sqlite3


# -------------------------------------------------------------------------/
# class CDbSuppL                                                           /
# -------------------------------------------------------------------------/
class CDbSuppL:
    """---"""
    def __init__(self, sDbPath):
        """ Constructor """
        self.sDbPath = sDbPath
        self.sError = 0
        self.sErrMsg = ""
        self.__con__ = None
        self.__cur__ = None

    # ---------------------------------------------------------
    # pfnGetConn
    # ---------------------------------------------------------
    def pfnGetConn(self):
        pass

    # ---------------------------------------------------------
    # fnDbConnect
    # ---------------------------------------------------------
    def fnDbConnect(self):
        self.__con__ = sqlite3.connect(self.sDbPath)
        self.__con__.isolation_level = None
        self.__con__.row_factory = sqlite3.Row
        self.__cur__ = self.__con__.cursor()

    # ---------------------------------------------------------
    # fnDbClose
    # ---------------------------------------------------------
    def fnDbClose(self):
        self.__con__.close()

    # ---------------------------------------------------------
    # afnQuery
    # ---------------------------------------------------------
    def afnQuery(self, sQ):
        """Returns result in list form"""
        result = []
        self.__cur__.execute(sQ)
        r = self.__cur__.fetchone()
        while r:
            d = {}
            for k in r.keys():
                d[k] = r[k]
            result.append(d)
            r = self.__cur__.fetchone()
        return result

    # ---------------------------------------------------------
    # fnQueryPar
    # ---------------------------------------------------------
    def afnQueryPar(self, sQ, *aPar):
        pass

    # ---------------------------------------------------------
    # fnCommCommit
    # ---------------------------------------------------------
    def fnCommCommit(self, sQ, *aPar):
        """Executes command from sQ and commit"""
        if (aPar):
            self.__cur__.execute(sQ, aPar)
        else:
            self.__cur__.execute(sQ)
        self.__con__.commit()

    # ---------------------------------------------------------
    # fnComm
    # ---------------------------------------------------------
    def fnComm(self, sQ):
        """Executes command sQ"""
        self.__cur__.execute(sQ)

    # ---------------------------------------------------------
    # fnCommit
    # ---------------------------------------------------------
    def fnCommit(self):
        """Sends commit to the db """
        self.__con__.commit()

    # ---------------------------------------------------------
    # sfnQueryRes
    # ---------------------------------------------------------
    def sfnQueryRes(self, sQ, sColSep, sRowSep):
        """ """
        result = ''
        a = self.afnQuery(sQ)
        for i in range(len(a)):
            onerow = ''
            if result != '':
                result = result + sRowSep
            for k, v in a[i].iteritems():
                if onerow != '':
                    onerow = onerow + sColSep
                onerow = onerow + v
            result = result + onerow
        return result


# ----------------------------------------------------------------------------
#                                   Test code
# ----------------------------------------------------------------------------
if __name__ == "__main__":
    cDb = CDbSuppL("test.sqlite")
    cDb.fnDbConnect()
    a = cDb.afnQuery(
            """
            select '1' as Suffix union
            select '2' as Suffix union
            select '3' as Suffix
            """)
    print(len(a))
    for i in range(len(a)):
        for k, v in a[i].iteritems():
            print(i, k, v)

    a = cDb.afnQuery("SELECT * FROM Params where Pattern='TestParam'")
    print(len(a))
    for i in range(len(a)):
        for k, v in a[i].iteritems():
            print(i, k, v)

    cDb.fnDbClose()
