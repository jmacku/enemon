#!/usr/bin/env python

# -------------------------------------------------------------------------/
#                                                                          /
#                           cdbsupplpy.py                                  /
#             Module for generic database access support                   /
#                                                                          /
# -------------------------------------------------------------------------/
#                                                                          /
# -------------------------------------------------------------------------/
#  This file implements the SQLite database behavior                       /
# -------------------------------------------------------------------------/
import psycopg2
# -------------------------------------------------------------------------/
# class CDbSuppL                                                           /
# -------------------------------------------------------------------------/
class CDbSuppL:
  """---"""
  def __init__(self, sDbConStr):
    """ Constructor """
    self.sDbConStr = sDbConStr
    self.sError  = 0
    self.sErrMsg = ""
    self.__con__ = None
    self.__cur__ = None

  #---------------------------------------------------------
  # pfnGetConn
  #---------------------------------------------------------
  def pfnGetConn(self):
    pass

  #---------------------------------------------------------
  # fnDbConnect
  #---------------------------------------------------------
  def fnDbConnect(self):
    self.__con__ = psycopg2.connect(self.sDbConStr)
    self.__cur__ = self.__con__.cursor()

  #---------------------------------------------------------
  # fnDbClose
  #---------------------------------------------------------
  def fnDbClose(self):
    self.__cur__.close()
    self.__con__.close()

  #---------------------------------------------------------
  # afnQuery
  #---------------------------------------------------------
  def afnQuery(self, sQ, *aPars):
    """Returns result in list form"""
    try:
        if(aPars):
            self.__cur__.execute(sQ, aPars)
        else:
            self.__cur__.execute(sQ)
        cols = list(self.__cur__.description)
        result = self.__cur__.fetchall()
    except (Exception, psycopg2.DatabaseError) as e:
        print(e)
        self.__cur__.close()
        exit(1)
    results = []
    for row in result:
        row_dict = {}
        for i, col in enumerate(cols):
            row_dict[col.name] = row[i]
        results.append(row_dict)
    return results

  #---------------------------------------------------------
  # fnQueryPar
  #---------------------------------------------------------
  def afnQueryPar(self, sQ, *aPars):
    pass

  #---------------------------------------------------------
  # fnCommCommit
  #---------------------------------------------------------
  def fnCommCommit(self, sQ, *aPar):
    """Executes command from sQ and commit"""
    if(aPar):
        self.__cur__.execute(sQ, aPar)
    else:
        self.__cur__.execute(sQ)
    self.__con__.commit()

  #---------------------------------------------------------
  # fnComm
  #---------------------------------------------------------
  def fnComm(self, sQ, *aPar):
    """Executes command sQ"""
    if(aPar):
        self.__cur__.execute(sQ, aPar)
    else:
        self.__cur__.execute(sQ)

  #---------------------------------------------------------
  # fnCommit
  #---------------------------------------------------------
  def fnCommit(self):
    """Sends commit to the db """
    self.__con__.commit()


  #---------------------------------------------------------
  # sfnQueryRes
  #---------------------------------------------------------
  def sfnQueryRes(self, sQ, sColSep, sRowSep):
    """ """
    result = ''
    a = self.afnQuery(sQ)
    for i in range(len(a)):
      onerow = ''
      if result != '':
        result = result + sRowSep
      for k in a[i].keys():
        if onerow != '':
          onerow = onerow + sColSep
        onerow = onerow + a[i].get(k)
      result = result + onerow
    return result

#----------------------------------------------------------------------------
#                                   Test code
#----------------------------------------------------------------------------
if __name__ == "__main__":
  cDb.fnDbConnect()
  a = cDb.afnQuery("select '1' as Suffix union select '2' as Suffix union select '3' as Suffix")
  print(len(a))
  for i in a:
      print(i)

  a = cDb.afnQuery("SELECT * FROM tenergy order by dt desc LIMIT 10;")
  print(len(a))
  for i in a:
      print(i)

  cDb.fnCommCommit("INSERT INTO test (id, txt) VALUES(%s, %s);", 2, 'hodnota 2')

  cDb.fnDbClose()
