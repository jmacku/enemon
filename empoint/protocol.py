#!/usb/bin/env python

import minimalmodbus
import http.client
import socket
import json

from point import cListOfPoints
from emplogger import Log
import constants


class cProtocol:
    """ """
    def __init__(self, redis_helper=None):
        """ """
        self.aPoints = cListOfPoints(redis_helper)
        self.boConnection = False
        self.sError = ''
        self.nMsgOK = 0
        self.nMsgKO_IO = 0
        self.nMsgKO_Val = 0
        self.nMsgKO_Typ = 0
        self.nMsgOKlst = 0
        self.nMsgKO_IOlst = 0
        self.nMsgKO_Vallst = 0
        self.nMsgKO_Typlst = 0

    def addPoint(self, sName, iType, iDec, iAddr, iDir, sUnit, iSync, sURL='',
                 sIP='localhost', iPort=80, iNodeID=1):
        """ """
        self.aPoints.addPoint(sName, iType, iDec, iAddr, iDir, sUnit, iSync,
                              sURL, sIP, iPort, iNodeID)

    def addPointObject(self, oP):
        """ """
        self.aPoints.addPointObject(oP)

    def fnGetPointByName(self, sName):
        """ """
        return self.aPoints.fnGetPointByName(sName)

    def fnClearErrCount(self):
        """ """
        self.nMsgOK = 0
        self.nMsgKO_IO = 0
        self.nMsgKO_Val = 0
        self.nMsgKO_Typ = 0

    def fnSwapErrCount(self):
        """ """
        self.nMsgOKlst = self.nMsgOK
        self.nMsgKO_IOlst = self.nMsgKO_IO
        self.nMsgKO_Vallst = self.nMsgKO_Val
        self.nMsgKO_Typlst = self.nMsgKO_Typ
        self.fnClearErrCount()

    def runPoint(self, oPoint):
        """ """
        pass

    def runPoints(self):
        """ """
        self.aPoints.read_point_list()
        for point in self.aPoints.iter_points_all():
            if (point.bofnGetSyncReq()):
                boFail = False
                try:
                    self.runPoint(point)
                    boFail = False
                    self.sError = ' ' * 50
                except IOError as e:
                    self.sError = e
                    boFail = True
                    self.nMsgKO_IO += 1
                except ValueError:
                    self.sError = "Wrong character - value!"
                    boFail = True
                    self.nMsgKO_Val += 1
                except TypeError:
                    self.sError = "Wrong character - type! %s" % (
                        point.sfnGetName())
                    boFail = True
                    self.nMsgKO_Typ += 1
                except (http.client.HTTPException,
                        http.client.InvalidURL,
                        socket.error) as ex:
                    self.sError = "%s" % (ex, )
                    boFail = True
                    self.nMsgKO_IO += 1
                    Log.error("%s (%s)" % (ex, point.sfnGetName()))
                if not boFail:
                    self.nMsgOK += 1
                    point.fnSetValid()
                    self.boConnection = True
                    if (not point.bofnIsPeriodic()):
                        point.fnSetSyncReq(False)
                else:
                    point.fnSetInvalid()
                    self.boConnection = False
        self.aPoints.write_point_list()


class cModbusProtocol(cProtocol):
    """ """
    def __init__(self, sDevice, redis_helper=None):
        cProtocol.__init__(self, redis_helper)
        self.sDevice = sDevice
        self.CommNodes = {}

    def createCommNode(self, iNodeID):
        """ """
        CommNode = minimalmodbus.Instrument(self.sDevice, iNodeID)
        # help to recover after noise on the line
        CommNode.precalculate_read_size = False
        self.CommNodes[iNodeID] = CommNode

    def runPoint(self, oPoint):
        """ """
        CommNode = self.CommNodes[oPoint.iNodeID]
        if oPoint.isDirIn():
            Log.debug("Processing N: %s A: %s: V: %s D: %s" % (
                oPoint.sfnGetName(),
                oPoint.iAddr,
                oPoint.gfnGetRawValue(),
                oPoint.iDir))
            if oPoint.isTypeInt() or oPoint.isTypeTarif():
                oPoint.Value = CommNode.read_register(oPoint.iAddr,
                                                      oPoint.iDec)
            if oPoint.isTypeLong() or oPoint.isTypeTime():
                oPoint.Value = CommNode.read_long(oPoint.iAddr)

        if oPoint.isDirOut():
            Log.debug("Processing N: %s A: %s: V: %s D: %s" % (
                oPoint.sfnGetName(),
                oPoint.iAddr,
                oPoint.gfnGetRawValue(),
                oPoint.iDir))
            if oPoint.isTypeInt() or oPoint.isTypeTarif():
                CommNode.write_register(oPoint.iAddr,
                                        float(oPoint.gfnGetRawValue()),
                                        oPoint.iDec)
            if oPoint.isTypeLong() or oPoint.isTypeTime():
                CommNode.write_long(oPoint.iAddr, int(oPoint.gfnGetRawValue()))


class cWebProtocol(cProtocol):
    """ """
    def __init__(self, sAddress='', iPort=80, redis_helper=None):
        cProtocol.__init__(self, redis_helper)
        self.sAddress = sAddress
        self.iPort = iPort

    def runPoint(self, oPoint):
        """ """
        if oPoint.isDirIn():
            if oPoint.isTypeZero():
                conn = http.client.HTTPConnection(oPoint.sIP, oPoint.iPort,
                                                  timeout=10)
                conn.request("GET", '/api-get?name='+oPoint.sURL)
                res = conn.getresponse()
                data = json.loads(res.read())
                if type(data) is list:
                    tmp_val = float("%.2f" % data[0][oPoint.sURL])
                else:
                    tmp_val = float("%.2f" % data[oPoint.sURL])
                oPoint.Value = float("%.2f" % (tmp_val, ))
            else:
                conn = http.client.HTTPConnection(oPoint.sIP, oPoint.iPort,
                                                  timeout=10)
                conn.request("GET", '/'+oPoint.sURL)
                res = conn.getresponse()
                data = json.loads(res.read())
                if type(data) is list:      # check for multiple items
                    oPoint.Value = float("%.2f" % (data[0][oPoint.sURL], ))
                else:
                    oPoint.Value = float("%.2f" % (data[oPoint.sURL], ))
            conn.close()
        if oPoint.isDirOut():
            conn = http.client.HTTPConnection(oPoint.sIP, oPoint.iPort,
                                              timeout=10)
            if oPoint.isTypeTarif():
                if oPoint.bofnIsLowTarif():
                    conn.request("GET", "/"+oPoint.sURL+'on')
                else:
                    conn.request("GET", "/"+oPoint.sURL+'off')
                res = conn.getresponse()
            if oPoint.isTypeLevel():
                if oPoint.Value < constants.TARGET_LEVEL_ALMOST_EMPTY:
                    conn.request("GET", "/tlevelempty")
                elif oPoint.Value < constants.TARGET_LEVEL_ALMOST_FULL:
                    conn.request("GET", "/tlevelaempty")
                elif oPoint.Value < constants.TARGET_LEVEL_FULL:
                    conn.request("GET", "/tlevelafull")
                else:
                    conn.request("GET", "/tlevelfull")
            if oPoint.isTypeZero():
                conn = http.client.HTTPConnection(oPoint.sIP, oPoint.iPort,
                                                  timeout=20)
                uri = "/api-set?%s=%s" % (oPoint.sURL, oPoint.Value)
                conn.request("GET", uri)
                res = conn.getresponse()
            if oPoint.isTypeZeroTarif():
                value = 1 if oPoint.bofnIsLowTarif() else 0
                conn = http.client.HTTPConnection(oPoint.sIP, oPoint.iPort,
                                                  timeout=20)
                uri = "/api-set?%s=%s" % (oPoint.sURL, value)
                conn.request("GET", uri)
                res = conn.getresponse()
            conn.close()
