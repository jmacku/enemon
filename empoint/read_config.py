#!/usr/bin/env python

import yaml
from point import cListOfPoints, cVisualPoint, cTMPPoint, cNTCPoint  # noqa F401
import constants


class cConfig:
    """" """
    def __init__(self, cfg_file):
        self.cfg_file = cfg_file
        self.points = None

    def load_config(self):
        with open(self.cfg_file) as file:
            cfg = yaml.safe_load(file)
        self.points = cfg['points']

    def get_points(self, protocol):
        pnts = [x for x in self.points if x['protocol'] == protocol]
        return pnts

    def get_points_all(self):
        pnts = [x for x in self.points]
        return pnts

    def add_point_to_list(self, listOfPoints, pointData):
        oPoint = None
        pointClass = pointData.get('class', 'cVisualPoint')
        constructor = globals()[pointClass]
        oPoint = constructor(
            pointData['name'],
            constants.TYPE_MAP[pointData['type']],
            pointData['decimal'],
            pointData['mb_addr'],
            constants.DIR_MAP[pointData['direction']],
            pointData['unit'],
            constants.SYNC_MAP[pointData['sync']],
            sURL=pointData['url'],
            sIP=pointData['ip_addr'],
            iPort=pointData['port'],
            iNodeID=pointData.get('node_id', None))
        if pointData.get('parrent', None):
            masterPoint = listOfPoints.fnGetPointByName(pointData['parrent'])
            oPoint.fnSetMasterPoint(masterPoint)
        listOfPoints.addPointObject(oPoint, pointData['protocol'])

    def add_points_to_list(self, listOfPoints):
        for point in self.get_points_all():
            self.add_point_to_list(listOfPoints, point)


if __name__ == '__main__':
    from protocol import cWebProtocol
    from redis_sapi import RedisHelper
    from emplogger import Log

    Log.info('Test')
    oCfg = cConfig('config.yml')
    oCfg.load_config()
    redis_helper = RedisHelper('localhost')
    redis_helper.connect_redis()
    oLoP = cListOfPoints(redis_helper=redis_helper)
    oWP = cWebProtocol(redis_helper=redis_helper)
    oCfg.add_points_to_list(oLoP)
    for point in oLoP.iter_points_prot('web'):
        oWP.addPointObject(point)
    oWP.runPoints()
    for x in oWP.aPoints.iter_points_all():
        print(f'point {x.sfnGetName()} value {x.gfnGetValue()}')
    print('done')
