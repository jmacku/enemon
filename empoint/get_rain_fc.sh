#!/usr/bin/env bash
#===============================================
#
#               get_rain_fc.sh
#
#  Returns the 24 hour rain forecast in mm
#  for given city from openweathermap.org
#
#  Params:
#  APPID - id for openweathermap.org
#  CITYID - id of the city from city.list.json
#  PATH - where to store the returned json file
#
#===============================================
if [[ $# -lt 3 ]]; then
  echo "Usage $(basename $0) APPID CITYID PATH"
  exit
fi
DT=$(date -u +%Y-%m-%dT%H:%M)
#--------------------------------------------------
# For obtaining city id run
# jq ".[] | select(.name==CITY_NAME)" city.list.json
APPID=$1
CITYID=$2
FNAME="$3/rain_${DT}.json"

API_URL="http://api.openweathermap.org/data/2.5/forecast"
CMD="wget -O ${FNAME} ${API_URL}?id=${CITYID}&APPID=${APPID}"
#-------------------
#-- get the forecast
$CMD
#---------------------------------
#-- obtain the sum of 24 hour rain
if [ $? -eq 0 ]; then
  RAIN=$(jq '[ .list| .[] | .rain | ."3h" ] | .[0:8] | add + 0.5 | floor' $FNAME)
else
  RAIN=-1
fi
echo $RAIN
