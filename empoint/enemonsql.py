#!/usr/bin/python
# -*- coding: utf-8 -*-


from sqlalchemy import create_engine
from sqlalchemy.sql import text

import datetime
import calendar
import pytz


class cEnemonSQL(object):
    """ """
    def __init__(self, sDbConnStr):
        self.conn_str = sDbConnStr
        self.engine = create_engine(
            self.conn_str,
            future=True)

    def fnStoreTemp(self, dt, f_temp_gar, f_temp_bm, f_temp_hw1, f_temp_hw2):
        """ """
        sQ = text("""
            INSERT INTO ttemperature (
                dt, temp_garage, temp_cellar, temp_heat01, temp_heat02
            ) VALUES (
                :dt, :gar_temp, :cellar_temp, :hw1_temp, :hw2_temp);
            """)
        with self.engine.connect() as con:
            con.execute(sQ, parameters=dict(
                dt=dt, gar_temp=f_temp_gar, cellar_temp=f_temp_bm,
                hw1_temp=f_temp_hw1, hw2_temp=f_temp_hw2)
            )
            con.commit()

    def fnStoreDetail(self, dt, iTarif, iPower, iSync, f_tout, f_tin,
                      boiler_status, rain_mm_curr=0):
        """ """
        sQ = text("""
            INSERT INTO tpower (
                dt, i_tarif, i_power, i_sync, f_tout, f_tin, boiler_status,
                rain_mm_curr
            ) VALUES (
                :dt, :i_tarif, :i_power, :i_sync, :f_tout, :f_tin,
                :boiler_status, :rain_mm_curr);
            """)
        with self.engine.connect() as con:
            con.execute(sQ, parameters=dict(
                dt=dt, i_tarif=iTarif, i_power=iPower, i_sync=iSync,
                f_tout=f_tout, f_tin=f_tin, boiler_status=boiler_status,
                rain_mm_curr=rain_mm_curr)
            )
            con.commit()

    def fnStoreEnergy(self, dt, f_energy_lt, f_energy_ht,
                      f_tin_avg, f_tout_avg, boiler_time_on):
        """ """
        sQ = text("""
            INSERT INTO tenergy (
                dt, f_energy_lt, f_energy_ht, f_tin_avg, f_tout_avg,
                boiler_time_on
            ) VALUES (
                :dt, :f_energy_lt, :f_energy_ht, :f_tin_avg, :f_tout_avg,
                :boiler_time_on);
            """)
        with self.engine.connect() as con:
            con.execute(sQ, parameters=dict(
                dt=dt, f_energy_lt=f_energy_lt, f_energy_ht=f_energy_ht,
                f_tin_avg=f_tin_avg, f_tout_avg=f_tout_avg,
                boiler_time_on=boiler_time_on)
            )
            con.commit()

    def fnStoreRain(self, dt, rain_mm_lst):
        """ """
        sQ = text("""
            INSERT INTO traingauge (
                dt, rain_mm_lst
            ) VALUES (
                :dt, :rain_mm_lst);
            """)
        with self.engine.connect() as con:
            con.execute(sQ, parameters=dict(
                dt=dt, rain_mm_lst=rain_mm_lst)
            )
            con.commit()

    def fnStoreCommStat(self, dt, i_ok_count, i_ko_io_count,
                        i_ko_val_count, i_ko_typ_count):
        """ """
        sQ = text("""
            INSERT INTO  tcommstat (
                dt, i_ok_count, i_ko_io_count, i_ko_val_count, i_ko_typ_count
            ) VALUES (
                :dt, :i_ok_count, :i_ko_io_count, :i_ko_val_count,
                :i_ko_typ_count);
            """)
        with self.engine.connect() as con:
            con.execute(sQ, parameters=dict(
                dt=dt, i_ok_count=i_ok_count, i_ko_io_count=i_ko_io_count,
                i_ko_val_count=i_ko_val_count, i_ko_typ_count=i_ko_typ_count)
            )
            con.commit()

    def fnWriteLog(self, dt, reporter, message):
        """ """
        sQ = text("""
            INSERT INTO tlog (dt, reporter, message)
            VALUES (:dt, :reporter, :message);
            """)
        with self.engine.connect() as con:
            con.execute(sQ, parameters=dict(
                dt=dt, reporter=reporter, message=message)
            )
            con.commit()

    def fnStoreTank(self, dt, i_level, i_pump,
                    i_mode, i_manctrl=0, i_status=0):
        """ """
        sQ = text("""
            INSERT INTO ttankhistory (
                dt, i_level, i_pump, i_mode, i_manctrl, i_status
            ) VALUES (:dt, :i_level, :i_pump, :i_mode, :i_manctrl, :i_status);
            """)
        with self.engine.connect() as con:
            con.execute(sQ, parameters=dict(
                dt=dt, i_level=i_level, i_pump=i_pump, i_mode=i_mode,
                i_manctrl=i_manctrl, i_status=i_status)
            )
            con.commit()

    def ifnGetRainForecast(self, max_offset=12):
        """ """
        sQ = text("""
            SELECT dt, rain_24h_mm
            FROM trainforecast ORDER BY dt DESC LIMIT 1;
            """)
        with self.engine.connect() as con:
            r = con.execute(sQ)
            if r is None:
                return (-1)
            rd = r.mappings().all()
            dt = rd[0]['dt']
            utc = pytz.utc
            if (type(dt) is str):
                try:
                    dtdt = datetime.datetime.strptime(dt, "%Y-%m-%dT%H:%M")
                    dt_delta = datetime.datetime.utcnow() - dtdt
                except Exception:
                    dt_delta = datetime.datetime.timedelta()
            else:
                dt_delta = datetime.datetime.now(utc) - dt
            # print(type(dtdt))
            # print(dtdt)
            # print(dt_delta)
            if max_offset*60*60 < dt_delta.total_seconds():
                rain = -1
            else:
                rain = rd[0]['rain_24h_mm']
            return rain

# CREATE  TABLE "main"."tLog" (
#     "dt" DATETIME PRIMARY KEY  NOT NULL,
#     "reporter" VARCHAR,
#     "message" TEXT)


# ---------------------------------------------------------------------------
#                          testovaci kod
# ---------------------------------------------------------------------------
if __name__ == "__main__":
    import sys
    cs = "postgresql+psycopg2://%s:%s@malina05/enemon" % (sys.argv[1],
                                                          sys.argv[2])
    # cs = "sqlite:///enemon.sqlite"

    oSQL = cEnemonSQL(cs)
    dt = datetime.datetime.now()
    ldt = calendar.timegm(dt.utctimetuple())

#  oSQL.fnStoreDetail(ldt, 0, 123, 1)
#  a = oSQL.afnQuery("SELECT * FROM tPower")
#  print len(a)
#  for i in range(len(a)):
#    for k, v in a[i].iteritems():
#      print i, k, v
#
#  oSQL.fnStoreEnergy(ldt, 0.0, 1.23)
#  a = oSQL.afnQuery("SELECT * FROM tEnergy")
#  print len(a)
#  for i in range(len(a)):
#    for k, v in a[i].iteritems():
#      print i, k, v
#
#  oSQL.fnStoreCommStat(ldt, 12, 1, 2, 3)
#  a = oSQL.afnQuery("SELECT * FROM tCommStat")
#  print len(a)
#  for i in range(len(a)):
#    for k, v in a[i].iteritems():
#      print i, k, v
#
#  oSQL.fnStoreTank(ldt, 3, 1, 1, 0, 0)
#  a = oSQL.afnQuery("SELECT * FROM tTankHistory")
#  print len(a)
#  for i in range(len(a)):
#    for k, v in a[i].iteritems():
#      print i, k, v
    rain = oSQL.ifnGetRainForecast()
    print("rain_24: %d mm" % (rain))
