#!/usr/bin/env bash
#============================================
#
#            store_rain_fc.sh
#
# Stores the rain forecast into table
# in sqlite database
#
# Params:
# DB_FILE - sqlite database
# RAIN_MM - value of the rain forecast in mm
#
# The folowing table is expected in the db:
#
# CREATE TABLE tRainForecast(
#     dt datetime primary key not null,
#     rain_24h_mm integer
# );
#
#============================================
if [[ $# -lt 2 ]]; then
  echo "Usage $(basename $0) DB_FILE RAIN_MM"
  exit
fi
DT=$(date -u +%Y-%m-%dT%H:%M)
DB_FILE=$1
RAIN=$2
#----------------------
#-- store into database
Q="insert into tRainForecast(dt, rain_24h_mm) values ('${DT}', $RAIN)"
sqlite3 "$DB_FILE" "$Q"
