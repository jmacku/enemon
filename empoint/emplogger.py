#!/usr/bin/env python

import logging
import logging.handlers
import time


class EmpLogger(object):
    """ """
    LOG_LEN = 100000
    BKP_CNT = 3
    DEBUG = logging.DEBUG
    INFO = logging.INFO
    WARNING = logging.WARNING
    ERROR = logging.ERROR
    CRITICAL = logging.CRITICAL

    def __init__(self):
        """ """
        self.logEngine = logging.getLogger('empoint')

    def getLogName(self):
        """ """
        return 'empoint.log'

    def setTextLogging(self, fName='', level=-1):
        """ """
        if level < 0:
            level = self.DEBUG
        if not fName:
            fName = self.getLogName()
        self.logEngine.setLevel(level=level)
        handler = logging.handlers.RotatingFileHandler(
            fName, maxBytes=self.LOG_LEN, backupCount=self.BKP_CNT)
        formatter = logging.Formatter(
            "%(asctime)s %(name)s %(levelname)s %(message)s")
        formatter.converter = time.gmtime
        handler.setFormatter(formatter)
        if not self.logEngine.handlers:
            self.logEngine.addHandler(handler)
            self.info('Logging started ---')

    def setLevel(self, level):
        """ """
        self.logEngine.setLevel(level=level)

    def debug(self, msg):
        """ """
        self.logEngine.debug(msg)

    def info(self, msg):
        """ """
        self.logEngine.info(msg)

    def warning(self, msg):
        """ """
        self.logEngine.warning(msg)

    def error(self, msg):
        """ """
        self.logEngine.error(msg)

    def critical(self, msg):
        """ """
        self.logEngine.critical(msg)


def createLogger():
    """ """
    return EmpLogger()


global Log


Log = createLogger()


def getLog():
    return Log


# =====================================================
if __name__ == '__main__':
    Log.setTextLogging(level=Log.DEBUG)
    print('Test logging')
    Log.debug("test debug message")
    Log.info("test info message")
    Log.warning("test warning message")
    Log.error("test error message")
    Log.critical("test critical message")
    print('End logging test')
