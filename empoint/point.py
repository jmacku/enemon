#!/usr/bin/env python

import curses
import datetime
import pytz
import math
import constants

utc = pytz.utc
aT = ["NT", "VT", "EL", "ED", "UN"]


class cPoint:
    """Measuring point """
    def __init__(self, sName, iType, iDec, iAddr, iDir, sUnit, iSync, sURL='',
                 sIP='localhost', iPort=80, iNodeID=1):
        self.sName = sName
        self.iType = iType              # .. INT/LONG/TIME/TARIF
        self.iDec = iDec                # .. decimal places
        self.iAddr = iAddr              # .. REG #
        self.iDir = iDir                # .. NO/IN/OUT
        self.sUnit = sUnit
        self.iSync = iSync              # .. PERIODIC/ONDEMAND
        self.sURL = sURL
        self.sIP = sIP
        self.iPort = iPort
        self.Value = 0
        self.boValid = False
        self.iNodeID = iNodeID          # .. id of the node of origine
        self.oMasterPoint = None
        if (self.iSync == constants.SYNC_PERIODIC):
            self.boSyncReq = True         # .. request to sync
        else:
            self.boSyncReq = False        # .. request to sync

    def fnSetValid(self):
        """ """
        self.boValid = True

    def fnSetInvalid(self):
        """ """
        self.boValid = False

    def bofnGetSyncReq(self):
        """ """
        return (self.boSyncReq)

    def fnSetSyncReq(self, boSyncReq):
        """ """
        self.boSyncReq = boSyncReq

    def bofnIsPeriodic(self):
        """ """
        return (self.iSync == constants.SYNC_PERIODIC)

    def sfnGetName(self):
        """ """
        return (self.sName)

    def fnSetValue(self, val):
        """ """
        self.Value = val.decode('utf-8') if type(val) == bytes else val

    def ifnGetDir(self):
        """ """
        return (self.iDir)

    def isDirIn(self):
        """ """
        return (self.iDir == constants.DIR_IN)

    def isDirOut(self):
        """ """
        return (self.iDir == constants.DIR_OUT)

    def isDirAll(self):
        """ """
        return (self.iDir == constants.DIR_ALL)

    def isTypeInt(self):
        """ """
        return (self.iType == constants.TYPE_INT)

    def isTypeLong(self):
        """ """
        return (self.iType == constants.TYPE_LONG)

    def isTypeTime(self):
        """ """
        return (self.iType == constants.TYPE_TIME)

    def isTypeTarif(self):
        """ """
        return (self.iType == constants.TYPE_TARIF)

    def isTypeLevel(self):
        """ """
        return (self.iType == constants.TYPE_LEVEL)

    def isTypeZero(self):
        """ """
        return (self.iType == constants.TYPE_ZERO)

    def isTypeZeroTarif(self):
        """ """
        return (self.iType == constants.TYPE_ZERO_TARIF)

    def gfnGetRawValue(self):
        """ Do not override this function """
        if self.oMasterPoint is not None:
            return self.oMasterPoint.gfnGetRawValue()
        else:
            return (self.Value)

    def gfnGetValue(self):
        """ """
        if self.oMasterPoint is not None:
            return self.oMasterPoint.gfnGetValue()
        else:
            val = self.Value
            if type(val) == int or type(val) == float:
                return val
            else:
                if val.isnumeric():
                    if int(val) == float(val):
                        return int(val)
                    else:
                        return float(val)
                else:
                    return val

    def fnSetMasterPoint(self, oMasterPoint):
        """ """
        self.oMasterPoint = oMasterPoint

    def bofnIsLowTarif(self):
        """ """
        if self.isTypeTarif() and not self.isTypeZeroTarif():
            return False
        v = self.gfnGetValue()
        if v == 0:
            return True
        else:
            return False


class cVisualPoint(cPoint):
    """ """
    def __init__(self, sName, iType, iDec, iAddr, iDir, sUnit, iSync, sURL='',
                 sIP='localhost', iPort=80, iNodeID=1):
        cPoint.__init__(self, sName, iType, iDec, iAddr, iDir, sUnit, iSync,
                        sURL, sIP, iPort, iNodeID)

    def fnShowPoint(self, iX, iY, oW):
        """ """
        attr = 0
        if (self.boValid):
            attr = curses.color_pair(1) | curses.A_BOLD
        else:
            attr = curses.color_pair(3)
        if self.isTypeTarif():
            oW.addstr(iY, iX, aT[int(self.Value)], attr)
        elif self.isTypeTime():
            dt = datetime.datetime.fromtimestamp(int(self.gfnGetValue()), utc)
            oW.addstr(iY, iX, dt.strftime("%d.%m.%Y %H:%M:%S"), attr)
        else:
            oW.addstr(iY, iX,
                      "{0:4.{n}f}".format(float(self.gfnGetValue()),
                                          n=self.iDec), attr)
            # oScreen.addstr(self.iR, self.iC, ("%X") % self.Value, attr)
        if (self.sUnit != ""):
            oW.addstr(iY, iX+5, "{0}".format(self.sUnit))


class cNTCPoint(cVisualPoint):
    """
        NTC termistor with 10k Ohm and B 3950
        The read value is voltage on the termistor.
        The upper resistor in the divider
        is 10k and the voltage is 1.25 V
    """
    ZeroK = 273.15
    B = 3950
    Ro = 10000
    Rd = 10000
    Un = 1.25
    To = 25.0 + ZeroK

    def gfnGetValue(self):
        # Value of the NTC resistace
        u_diff = self.Un - float(self.Value)
        if u_diff == 0:
            return -99.99
        res = float(self.Value)*self.Rd/(self.Un-float(self.Value))
        if res <= 0:
            return -99.99
        temp = (self.To * self.B /
                (self.B + self.To * math.log(res/self.Ro))) - self.ZeroK
        return round(temp, 2)

    def fnShowPoint(self, iX, iY, oW):
        """ """
        attr = 0
        if (self.boValid):
            attr = curses.color_pair(1) | curses.A_BOLD
        else:
            attr = curses.color_pair(3)
        oW.addstr(iY, iX,
                  "{0:4.{n}f}".format(self.gfnGetValue(), n=1), attr)
        if (self.sUnit != ""):
            oW.addstr(iY, iX+5, "{0}".format(self.sUnit))


class cTMPPoint(cVisualPoint):
    """
        TMP36 linear thermometer
        The read value is output voltage.
        The outpus is 10mV/K with 0.5V at O C.
    """
    k_temp = 100
    q_temp = -50

    def gfnGetValue(self):
        # Value of the NTC resistace
        temp = float(self.Value)*self.k_temp + self.q_temp
        return round(temp, 2)

    def fnShowPoint(self, iX, iY, oW):
        """ """
        attr = 0
        if (self.boValid):
            attr = curses.color_pair(1) | curses.A_BOLD
        else:
            attr = curses.color_pair(3)
        oW.addstr(iY, iX,
                  "{0:4.{n}f}".format(self.gfnGetValue(), n=1), attr)
        if (self.sUnit != ""):
            oW.addstr(iY, iX+5, "{0}".format(self.sUnit))


class cListOfPoints(object):
    """ """
    def __init__(self, redis_helper=None):
        self.aPoints = []
        self.redis_helper = redis_helper

    def addPoint(self, sName, iType, iDec, iAddr, iDir, sUnit, iSync, sURL='',
                 sIP='localhost', iPort=80, iNodeID=1, sProt=''):
        """ """
        P = cVisualPoint(sName, iType, iDec, iAddr, iDir, sUnit, iSync, sURL,
                         sIP, iPort, iNodeID)
        self.aPoints.append({'point': P, 'prot': sProt})

    def addPointObject(self, oP, sProt=''):
        """ """
        self.aPoints.append({'point': oP, 'prot': sProt})

    def fnGetPointByName(self, sName):
        """ """
        for i in range(0, len(self.aPoints)):
            if self.aPoints[i]['point'].sName == sName:
                return self.aPoints[i]['point']
        return None

    def iter_points_all(self):
        for point in self.aPoints:
            yield point['point']

    def iter_points(self, point_dir):
        for point in self.aPoints:
            if (point['point'].ifnGetDir() & point_dir) != 0:
                yield point['point']

    def iter_points_prot(self, sProt):
        for point in self.aPoints:
            if point['prot'] == sProt:
                yield point['point']

    def read_point_list(self, direction=constants.DIR_OUT):
        """
        """
        values = self.redis_helper.read_redis(
            [x.sfnGetName() for x in
                self.iter_points(direction)]
        )
        for val, point in zip(
                values, (x for x in self.iter_points(direction))):
            point.fnSetValue(val)

    def write_point_list(self, direction=constants.DIR_IN):
        self.redis_helper.write_redis(
            [x.sfnGetName() for x in self.iter_points(direction)],
            [x.gfnGetRawValue() for x in self.iter_points(direction)]
        )
