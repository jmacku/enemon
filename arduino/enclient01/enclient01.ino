//================================================================//
//                                                                //
//                        ENERGY MONITOR                          //
//          Tank and temperature client based on arduino          //
//                                                                //
//----------------------------------------------------------------//
//  Client for enemon based on arduino with ethernet shield
//
//  Functions 
//    - measure temperature from connected analog probe
//    - keep tank full with minimal energy cost - switch the
//      pump on only during low tarif period
//    The tarif state sends the master (empoint)
//    In AUTO mode the pump is activated only in case the tank
//    is not full and the tarif is LOW. In MAN mode the pump is
//    activated via REST API only when the tank is not full. 
//    Default operation mode is AUTO.
//
//  Communication is based on simple REST like API
//   
//  Only HTTP GET method is used
//    Read commands
//      temp1 - read the temperature (fp number in centigrades)                   
//      tank1 - read the level in tank (0-7 .. empty to full)
//      pump1 - read the pump state (0/1 .. OFF/ON)
//      tarif1 - read the current tariff (0/1 .. HIGH/LOW)
//      mode1 - read the control mode (0/1 .. MAN/AUTO)
//      target1 - read the target tank level (0-4)
//    Write commands
//      nton - set tariff to LOW
//      ntoff - set tariff to HIGH
//      pumpon - set pump to ON
//      pumpoff - set pump to OFF
//      modeon - set mode to AUTO
//      modeoff - set mode to MAN
//      tlevelempty - set target level to TANK_LEVEL_EMPTY
//      tlevelaempty - set target level to TANK_LEVEL_ALMOST_EMPTY 
//      tlevelafull - set target level to TANK_LEVEL_ALMOST_FULL 
//      tlevelfull - set target level to TANK_LEVEL_FULL 
//
//  Commands could be chained. Data are returned in json format.
//
//  Example 1
//    curl -s http://node/temp1/tank1/pump1 | jq .
//  will return something like this:
//    [
//      {
//        "temp1": 31.08
//      },
//      {
//        "tank1": 3
//      },
//      {
//        "pump1": 0
//      }
//    ]
//  Example 2
//    curl -s http://node/pumpon | jq .
//  will return something like this:
//    [
//      {
//        "pumpon": "OK"
//      }
//    ]
//
//  INPUTS
//    ANALOG_INPUT .. temperature
//    WL_LOW_PIN   .. water level low 
//    WL_MID_PIN   .. water level middle 
//    WL_HIGH_PIN  .. water level high 
//  OUTPUTS
//    WL_PUMP_PIN  .. pump activate 
//================================================================//

#include <SPI.h>
#include <Ethernet.h>


#define REQ_BUF_SZ   60
#define ANALOG_INPUT  0
#define FILTER_SIZE  50


#define WL_LOW_PIN     30
#define WL_MID_PIN     32
#define WL_HIGH_PIN    34
#define WL_PUMP_PIN    36

#define NTon  1
#define NToff 0

#define MODE_MANUAL 0
#define MODE_AUTOMATIC 1

//-- Tank level sensor weights --
//-- The tank level status should be sum of weights of flooded sensors
#define TANK_LEVEL_EMPTY 0
#define TANK_LEVEL_ALMOST_EMPTY 1
#define TANK_LEVEL_ALMOST_FULL 2
#define TANK_LEVEL_FULL 4

#define PUMP_OFF 0
#define PUMP_ON 1

int iTankStatus = TANK_LEVEL_EMPTY+TANK_LEVEL_ALMOST_EMPTY+TANK_LEVEL_ALMOST_FULL+TANK_LEVEL_FULL;
int iPumpReq = PUMP_OFF;
int iPumpStatus = PUMP_OFF;
int iNTStatus = NToff;
int iModeStatus = MODE_AUTOMATIC;
int iTankLevelTarget = TANK_LEVEL_ALMOST_FULL;
bool boPause = false;

int aFilter[FILTER_SIZE] = {0};                 // filter temp readings
int iFilterPos = 0;

unsigned long lNow;

#define TYPE_CYCLIC 0
#define TYPE_ONDEMAND 1
#define STATE_RUNNING 0
#define STATE_SLEEPING 1

#define TASKS_NUM 4

struct Task {
  int iTaskID;                                 // task identification
  unsigned long lPeriod;                       // task's period in ms
  unsigned long lLastRun;
  unsigned long lNextRun;                      // last run time stamp in ms
  unsigned long lDuration;                     // time consumed by this task
  byte bType;
  byte bState;
  void (*fnCallBack)(int);   
};

enum {
  TASK_TANK_READ,
  TASK_TANK_AUTO,
  TASK_TEMP,
  TASK_TANK_TIM,
  TASK_NUM
};

struct Task stTasks[TASK_NUM];


byte mac[] = {
  0x00, 0xA0, 0x24, 0x33, 0xFD, 0x0F  // mega
};

IPAddress ip(192, 168, 4, 100);  // mega
IPAddress subnet(255, 255, 255, 128);
IPAddress gateway(192, 168, 4, 1);
IPAddress myDns(192, 168, 3, 1);
 
// Initialize the Ethernet server library
// with the IP address and port you want to use
// (port 80 is default for HTTP):
EthernetServer server(80);

char HTTP_req[REQ_BUF_SZ] = {0};
char req_index = 0;

bool bofnReachedLevel(int iStatus, int iLevel){
  bool boResult = iStatus & iLevel ? true : false;
  return boResult;
}

void fnFillBuffer(int iTemp) {
  for(int i=0; i<FILTER_SIZE; i++) {
    aFilter[i] = iTemp;
  }
}

void fnAddToFilter(int iTemp) {
  aFilter[iFilterPos++] = iTemp;
  if(iFilterPos >= FILTER_SIZE) {
    iFilterPos = 0;
  }
}

float gfnGetFilterValue() {
  long lSum = 0; 
  for(int i=0; i<FILTER_SIZE; i++) {
    lSum += aFilter[i];
  }
  return(lSum*1.0/FILTER_SIZE);
}

float gfnGetTemp() {
  int sensorFiltered = gfnGetFilterValue();
  return(110.0*sensorFiltered/1023 - 50.0);   // bipolar temperature
}

float gfnGetTank() {
  return(iTankStatus * 1.0);
}

float gfnGetPump() {
  return(iPumpStatus * 1.0);
}

float gfnGetTarif() {
  return(iNTStatus * 1.0);
}

float gfnGetMode() {
  return(iModeStatus * 1.0);
}

float gfnGetTankLevelTarget() {
  return(iTankLevelTarget * 1.0);
}

int ifnSetTarif(int iT) {
  if((iT == NTon) || (iT == NToff)) {
    iNTStatus = iT;
    return 0;
  }
  else {
    return 1;
  }
}

int ifnSetPump(int iP) {
  if((iP == 0) || (iP == 1)) {
    iPumpReq = iP;
    return 0;
  }
  else {
    return 1;
  }
}

int ifnSetMode(int iM) {
  if((iM == 0) || (iM == 1)) {
    iModeStatus = iM;
    return 0;
  }
  else {
    return 1;
  }
}

int ifnSetTankTargetLevel(int iT) {
  if((iT >= TANK_LEVEL_EMPTY) && (iT <= TANK_LEVEL_FULL)) {
    iTankLevelTarget = iT;
    return 0;
  }
  else {
    return 1;
  };
}

// Stop pump if target level is reached - in manual mode full tank is always possible
void fnCheckPumpEnabled() {   
  int iTLT;
  iTLT = iModeStatus == MODE_MANUAL ? TANK_LEVEL_FULL : iTankLevelTarget; 
  if(bofnReachedLevel(iTankStatus, iTLT)) {
    iPumpReq = 0;
  }
  return;
}

void fnSendHeadResp(EthernetClient* client) {
  client->println("HTTP/1.1 200 OK");
  client->println("Content-Type: application/json");
// the connection will be closed after completion of the response
  client->println("Connection: close");  
  client->println();
  client->println("[");
}

void fnSendValueResp(EthernetClient* client, float fValue, char* tag, int iOrd) {
  if(iOrd > 1) {
    client->print(",");
  }
  client->print("{\"");
  client->print(tag);
  client->print("\":");
  client->print(fValue);
  client->println("}");
}

void fnSendCommConf(EthernetClient* client, int iValue, char* tag, int iOrd) {
  if(iOrd > 1) {
    client->print(",");
  }
  client->print("{\"");
  client->print(tag);
  client->print("\":");
  if(iValue == 0) {
    client->print("\"OK\"");
  }
  else {
    client->print("\"error\"");
  }
  client->println("}");
}

void fnSendErrorResp(EthernetClient* client) {
  client->print("{\"error\": \"unknown command\"}");
}

void fnCloseJson(EthernetClient* client) {
  client->println("]");
}

void fnSaveHTTPReq(char c) {
  if (req_index < (REQ_BUF_SZ - 1)) {
     HTTP_req[req_index++] = c;
  }
}

// reset the req buffer
void fnResetReqBuffer() {
  req_index = 0;                                       
  HTTP_req[0] = '\0';
}

void fnProcessReq(EthernetClient* client) {
  char c;
  bool boFound = false;
  int iNum = 0;
  
  if (*client) {
    boolean currentLineIsBlank = true;
    while (client->connected()) {
      if (client->available()) {
        c = client->read();
        fnSaveHTTPReq(c);
        if (c == '\n' && currentLineIsBlank) {
          fnSendHeadResp(client);                        // send a standard http response header  
          if(strstr(HTTP_req, "temp1")) {                     // check request
            fnSendValueResp(client, gfnGetTemp(), "temp1", ++iNum);   // send value
            boFound = true;
          }
          if(strstr(HTTP_req, "tank1")){
            fnSendValueResp(client, gfnGetTank(), "tank1", ++iNum);   // send value            
            boFound = true;
          }
          if(strstr(HTTP_req, "pump1")){
            fnSendValueResp(client, gfnGetPump(), "pump1", ++iNum);   // send value            
            boFound = true;
          }
          if(strstr(HTTP_req, "tarif1")){
            fnSendValueResp(client, gfnGetTarif(), "tarif1", ++iNum); // send value            
            boFound = true;
          }
          if(strstr(HTTP_req, "mode1")){
            fnSendValueResp(client, gfnGetMode(), "mode1", ++iNum); // send value            
            boFound = true;
          }
          if(strstr(HTTP_req, "target1")){
            fnSendValueResp(client, gfnGetTankLevelTarget(), "target1", ++iNum); // send value            
            boFound = true;
          }
          if(strstr(HTTP_req, "nton")){
            fnSendCommConf(client, ifnSetTarif(NTon), "nton", ++iNum);  // send value            
            boFound = true;
          }
          if(strstr(HTTP_req, "ntoff")){
            fnSendCommConf(client, ifnSetTarif(NToff), "ntoff", ++iNum);  // send value            
            boFound = true;
          }
          if(strstr(HTTP_req, "pumpon")){
            fnSendCommConf(client, ifnSetPump(1), "pumpon", ++iNum);  // send value            
            boFound = true;
          }
          if(strstr(HTTP_req, "pumpoff")){
            fnSendCommConf(client, ifnSetPump(0), "pumpoff", ++iNum);  // send value            
            boFound = true;
          }
          if(strstr(HTTP_req, "modeon")){
            fnSendCommConf(client, ifnSetMode(1), "modeon", ++iNum);  // send value            
            boFound = true;
          }
          if(strstr(HTTP_req, "modeoff")){
            fnSendCommConf(client, ifnSetMode(0), "modeoff", ++iNum);  // send value            
            boFound = true;
          }
          if(strstr(HTTP_req, "tlevelempty")){
            fnSendCommConf(client, ifnSetTankTargetLevel(TANK_LEVEL_EMPTY), "tlevelempty", ++iNum);  // send value            
            boFound = true;
          }
          if(strstr(HTTP_req, "tlevelaempty")){
            fnSendCommConf(client, ifnSetTankTargetLevel(TANK_LEVEL_ALMOST_EMPTY), "tlevelaempty", ++iNum);  // send value            
            boFound = true;
          }
          if(strstr(HTTP_req, "tlevelafull")){
            fnSendCommConf(client, ifnSetTankTargetLevel(TANK_LEVEL_ALMOST_FULL), "tlevelafull", ++iNum);  // send value            
            boFound = true;
          }
          if(strstr(HTTP_req, "tlevelfull")){
            fnSendCommConf(client, ifnSetTankTargetLevel(TANK_LEVEL_FULL), "tlevelfull", ++iNum);  // send value            
            boFound = true;
          }
          if(!boFound) {  
            fnSendErrorResp(client);                     // send error - bad req 
          }
          fnCloseJson(client);
          break;
        }
        if (c == '\n') {
          currentLineIsBlank = true;
        }
        else if (c != '\r') {
          currentLineIsBlank = false;                    // you've gotten a character on the current line
        }
      }
    }
    delay(1);                                            // give the web browser time to receive the data
    client->stop();                                      // close the connection:
    fnResetReqBuffer();                                  // reset the req buffer
  }
}

//================================================================================================
void setup() {
  // start the Ethernet connection and the server:
  Ethernet.begin(mac, ip, myDns, gateway, subnet);
  analogReference(INTERNAL1V1);  // mega

  server.begin();

  pinMode(WL_LOW_PIN, INPUT);
  pinMode(WL_MID_PIN, INPUT);
  pinMode(WL_HIGH_PIN, INPUT);
  pinMode(WL_PUMP_PIN, OUTPUT);
  pinMode(13, OUTPUT);

  // Task definition
  stTasks[TASK_TANK_READ].iTaskID = TASK_TANK_READ;                   // read tank status
  stTasks[TASK_TANK_READ].lPeriod = 1000;
  stTasks[TASK_TANK_READ].lNextRun = 100;
  stTasks[TASK_TANK_READ].lDuration = 0;
  stTasks[TASK_TANK_READ].bType = TYPE_CYCLIC;
  stTasks[TASK_TANK_READ].bState = STATE_RUNNING;
  stTasks[TASK_TANK_READ].fnCallBack = &fnTask_TANK_READ;

  stTasks[TASK_TANK_AUTO].iTaskID = TASK_TANK_AUTO;                   // auto mode 
  stTasks[TASK_TANK_AUTO].lPeriod = 5000;
  stTasks[TASK_TANK_AUTO].lNextRun = 1000;
  stTasks[TASK_TANK_AUTO].lDuration = 0;
  stTasks[TASK_TANK_AUTO].bType = TYPE_CYCLIC;
  stTasks[TASK_TANK_AUTO].bState = STATE_RUNNING;
  stTasks[TASK_TANK_AUTO].fnCallBack = &fnTask_TANK_AUTO;

  stTasks[TASK_TEMP].iTaskID = TASK_TEMP;                             // read temperature
  stTasks[TASK_TEMP].lPeriod = 3000;
  stTasks[TASK_TEMP].lNextRun = 220;
  stTasks[TASK_TEMP].lDuration = 0;
  stTasks[TASK_TEMP].bType = TYPE_CYCLIC;
  stTasks[TASK_TEMP].bState = STATE_RUNNING;
  stTasks[TASK_TEMP].fnCallBack = &fnTask_TEMP;

  stTasks[TASK_TANK_TIM].iTaskID = TASK_TANK_TIM;                     // pump timing
  stTasks[TASK_TANK_TIM].lPeriod = 180000l;
  stTasks[TASK_TANK_TIM].lNextRun = 150;
  stTasks[TASK_TANK_TIM].lDuration = 0;
  stTasks[TASK_TANK_TIM].bType = TYPE_CYCLIC;
  stTasks[TASK_TANK_TIM].bState = STATE_RUNNING;
  stTasks[TASK_TANK_TIM].fnCallBack = &fnTask_TANK_TIM;
  
  // Initialize temperature reading 
  analogRead(ANALOG_INPUT);
  delay(10);
  int iT = analogRead(ANALOG_INPUT);
  fnFillBuffer(iT);
}

//====================================================================
// Main loop
void loop() {
  fnScheduler();  
}

/*====================================================================*/
/*                                                                    */
/* Scheduler                                                          */  
/*                                                                    */ 
/*====================================================================*/

void fnScheduler()
{
  unsigned long lNow = millis();
  unsigned long lTmp;
  
  for(int i=0; i<TASK_NUM; i++) {
    if(stTasks[i].bState == STATE_RUNNING){
      if(stTasks[i].lNextRun <= lNow) {
        stTasks[i].lLastRun = lNow; 
        lTmp = micros();
        stTasks[i].fnCallBack(stTasks[i].iTaskID);
        stTasks[i].lDuration = micros()-lTmp;
        if(stTasks[i].bType == TYPE_CYCLIC){  
          stTasks[i].lNextRun += stTasks[i].lPeriod;
        }
        else{
          stTasks[i].bState = STATE_SLEEPING;
        }
      }
    }  
  }
  EthernetClient client = server.available();
  fnProcessReq(&client);
}

/*====================================================================*/
/*                                                                    */
/* Tasks implementation                                               */  
/*                                                                    */ 
/*====================================================================*/
void fnTask_TANK_READ(int iID)
{
  int s = 0;
  
  if(digitalRead(WL_LOW_PIN)) {
    s += 1;
  }
  if(digitalRead(WL_MID_PIN)) {
    s += 2;
  }
  if(digitalRead(WL_HIGH_PIN)) {
    s += 4;
  }
  iTankStatus = s;
  fnCheckPumpEnabled();
  if(iPumpReq > 0 && !boPause) {
    iPumpStatus = 1;
  }
  else {
    iPumpStatus = 0;
  }
  if(iPumpStatus  > 0) {
    digitalWrite(WL_PUMP_PIN, HIGH);
  }
  else {
    digitalWrite(WL_PUMP_PIN, LOW);
  }
  // visualize the pump pause
  if(boPause) {
    digitalWrite(13, HIGH);
  }
  else {
    digitalWrite(13, LOW);
  }
  return;
}

/*====================================================================*/
void fnTask_TANK_AUTO(int iID)
{
  if(iModeStatus == MODE_AUTOMATIC && iNTStatus == NTon && iTankStatus < 4) {
    iPumpReq = 1;
  }
  if(iModeStatus == MODE_AUTOMATIC && iNTStatus != NTon) {
    iPumpReq = 0;
  }
  return;
}

/*====================================================================*/
void fnTask_TEMP(int iID)
{
  int iT = analogRead(ANALOG_INPUT);
  fnAddToFilter(iT);
  return;
}

/*====================================================================*/
void fnTask_TANK_TIM(int iID)            // pump timing
{
  if(boPause) {
    boPause = false;
  }
  else {
    boPause = true;
  }
  return;
}
