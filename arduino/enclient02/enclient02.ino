//================================================================//
//                                                                //
//                        ENERGY MONITOR                          //
//          Energy consumption reader with LCD and modbus         //
//                                                                //
//----------------------------------------------------------------//
//  20.09.2014 - v2.0 - implemented with cooperative multitasking //
//  15.11.2014 - v2.1 - without file save support                 //
//  15.11.2014 - v2.2 - modbus slave                              //
//  23.11.2014 - v2.3 - time sync from master                     //
//  12.12.2014 - v2.4 - archive support                           //
//================================================================//
//  Client for enemon based on arduino with LCD and RTC
//
//  Functions
//    - read the flashing LED from electricity meter for 1 minute
//    - counts the current minute consuption in Watts   
//    - reads the LED tariff of electricity meter (LOW/HIGH)
//    - evaluates the hourly consumptions
//    - sends values to master via modbus RTC
//    - shows values on 2x16 LCD display
//    - synchronize local RTC clock from master

//#define MEMORY_TEST
//#define DEBUG_LIST
#define MODBUS


#include <SimpleModbusSlave.h>
#include <LiquidCrystal.h>
#include <Wire.h>
#include "RTClib.h"

#ifdef MEMORY_TEST
#include <MemoryFree.h>
#endif


#define ledPIN 13
#define dataEnable 12
#define boilerPIN 11
#define stationID 1
#define pulsePIN 2
#define ntPIN 0 
#define vtPIN 1
#define pulseInterrupt 0

// Tariff and consumption LED calibration
#define darkLevel 100                  // less then means LED off
#define lghtLevel 200                  // greater then means LED on

// Tariff mapping
#define tNT 0
#define tVT 1
#define tEL 2
#define tED 3
#define tUN 4

#define DEBOUNCE_LIMIT 20              // min 20 ms between pulses 

//-- Modbus registers --
enum 
{     
  // just add or remove registers and your good to go...
  // The first register starts at address 0
  A_TIME_HW,         // unix time high word         
  A_TIME_LW,         // unix time low word
  TARIF,             // 0 -low, 1 - high
  POWER,             // current power (W)
  LT_ENERGY_LST,     // last hour energy consumption in LT (kWh) * 100
  HT_ENERGY_LST,     // last hour energy consumption in HT (kWh) * 100
  LT_ENERGY_CUR,     // current hour energy consumption in LT (kWh) * 100
  HT_ENERGY_CUR,     // current hour energy consumption in HT (kWh) * 100
  M_TIME_HW,         // reference time of master host - high word      
  M_TIME_LW,         // regerence time of master host - low word     
  STIME_FLAG,        // flag for time sync 
  RQ_HIST_TIME_HW,   // request timestamp from archive (high word)
  RQ_HIST_TIME_LW,   // request timestamp from archive (low word)
  RS_HIST_TIME_HW,   // response timestamp from archive (high word)
  RS_HIST_TIME_LW,   // response timestamp from archive (low word)
  RS_HIST_LT_ENER,   // response timestamp energy consumption in LT (kWh) * 100 
  RS_HIST_HT_ENER,   // response timestamp energy consumption in HT (kWh) * 100 
  ARCH_DEPTH,        // actual depth of the archive
  GAS_BOILER_STATUS, // gas boiler on/off
  GAS_BOILER_TIME,   // gas boiler on time during last hour 0-60 (min)
  HOLDING_REGS_SIZE  // leave this one
  // total number of registers for function 3 and 16 share the same register array
  // i.e. the same address space
};

unsigned int holdingRegs[HOLDING_REGS_SIZE]; // function 3 and 16 register array


//- Cooperative multitasking --------------------------------------
struct Task {
   int iTaskID;                        // task identification
   unsigned long lPeriod;              // task's period in ms
   unsigned long lLastRun;
   unsigned long lNextRun;             // last run time stamp in ms
   unsigned long lDuration;            // time consumed by this task
   void (*fnCallBack)(int);   
};

//- tasks ----------------------------------------------------------
enum {
  TASK_SECOND,
  TASK_DISPLAY,
  TASK_LOG,
#ifdef DEBUG_LIST  
  TASK_DEBUG,
#endif  
#ifdef MODBUS  
  TASK_MODBUS_ARCH,
#endif  
  TASK_SYNCTIME,
  TASK_GAS_BOILER,
  NUMBER_OF_TASKS 
};  

struct Task stTasks[NUMBER_OF_TASKS];

//- archive --------------------------------------------------------
#define ARCHIVE_SIZE 100             // len of archive (cca 4 days)

struct Archive {
  unsigned long lTimestamp;          // begining of hour in UTC
  unsigned int nEnergyLT;            // low tarif energy * 100
  unsigned int nEnergyHT;            // high tarif energy * 100
};

struct Archive stArchive[ARCHIVE_SIZE];

byte bArcTailPtr=0;
byte bArcHeadPtr=0;

//- other variables ------------------------------------------------
LiquidCrystal lcd(9, 8, 7, 6, 5, 4);   // pin assigment
RTC_DS1307 RTC;

char sLCDDate[12];                     // 18.04 22:13
char sTarif[3] = {'!', '-', '\0'};

byte bTarif = tUN;                     // 
int ntVal;
int vtVal;

int iCurPower;                         // actual power (Watt)
unsigned int nLastCount = 0;
float fEnergyNT = 0;
float fEnergyVT = 0;
float fEnergyNTlst = 0;
float fEnergyVTlst = 0;

boolean boSaving = false;
DateTime curTime;

int iGasBoilerTimeOn = 0;

long lLastInt = 0;

volatile unsigned int nPulseCounter;


//======================================================================================
// Support function
//--------------------------------------------------------------------------------------
void fnAddToArchive(unsigned long lTime, unsigned int nEnergyLT, unsigned int nEnergyHT)
{
  stArchive[bArcTailPtr].lTimestamp = lTime;
  stArchive[bArcTailPtr].nEnergyLT = nEnergyLT;
  stArchive[bArcTailPtr].nEnergyHT = nEnergyHT;
  bArcTailPtr++;
  if(bArcTailPtr > ARCHIVE_SIZE-1) {
    bArcTailPtr = 0;
  }
  if(bArcTailPtr == bArcHeadPtr) {   // Full buffer
    bArcHeadPtr++;
  }  
  if(bArcHeadPtr > ARCHIVE_SIZE-1) {
    bArcHeadPtr = 0;
  }  
}


byte bfnGetArchiveLen()
{
  if(bArcTailPtr > bArcHeadPtr) {
    return(bArcTailPtr - bArcHeadPtr);
  }
  else if(bArcTailPtr < bArcHeadPtr) {
    return(ARCHIVE_SIZE + 1 + bArcTailPtr - bArcHeadPtr); 
  }  
  else {
    return 0;  
  }
}  

byte bfnGetArchPosition(unsigned long lTime) 
{
  for(byte bPos=0; bPos < ARCHIVE_SIZE; bPos++) {
    if(stArchive[bPos].lTimestamp == lTime) {
       return (bPos);
    }   
  }  
  return(ARCHIVE_SIZE+1);
}  
  

void fnClearRegs()
{
  for(int i=0; i<HOLDING_REGS_SIZE; i++) {
    holdingRegs[i] = 0;
  }  
}  

void fnGetLCDDate(DateTime pTime, char* sTime)
{
  int n;
  
  n=pTime.day();
  sTime[0] = '0' + (n/10);
  sTime[1] = '0' + (n%10);
  sTime[2] = '.';
  n=pTime.month();
  sTime[3] = '0' + (n/10);
  sTime[4] = '0' + (n%10);
  sTime[5] = ' ';
  n=pTime.hour();
  sTime[6] = '0' + (n/10);
  sTime[7] = '0' + (n%10);
  sTime[8] = ':';
  n=pTime.minute();
  sTime[9] = '0' + (n/10);
  sTime[10] = '0' + (n%10);
  sTime[11] = '\0';
}



void fnAlignNum(int iNum, int iLen, char* sNum)
{
  int iTmp, i;
  
  if(iNum == 0) {
    for(i=0; i<iLen-1; i++) {
      sNum[i]=' ';
    }  
    sNum[iLen-1] = '0';
  }  
  else {
    iTmp = iNum;
    for(i=iLen; i>0; i--) {
      if(iTmp > 0) {
        sNum[i-1] = '0' + (iTmp%10);
      }
      else {
        sNum[i-1] = ' ';
      }  
      iTmp = iTmp / 10;
    }  
  }  
  sNum[iLen] = '\0';  
}  


void fnDisplayDateTime(DateTime pTime, boolean boShow=false)
{
  if((pTime.second() == 0) || boShow) {
    fnGetLCDDate(pTime, sLCDDate);
    lcd.setCursor(0,0);  
    lcd.print(sLCDDate);
  }  
}


void fnDisplaySaving()
{
  if(boSaving) {
    lcd.setCursor(12, 0);
    lcd.print('*');
    boSaving = false;
  }
  else {
    lcd.setCursor(12, 0);
    lcd.print(' ');
  }  
}


void fnGetCurPower()
{
  noInterrupts();
  nLastCount = nPulseCounter;
  nPulseCounter = 0;
  interrupts();
  iCurPower = 60*nLastCount*2;       // log interval 60s
  if(bTarif == tNT) {
    fEnergyNT += iCurPower/60000.0;
  }  
  if(bTarif == tVT) {
    fEnergyVT += iCurPower/60000.0;
  }  
} 


byte bfnGetTarif(char* sTar)
{
  byte bRetVal;
  
  if ((vtVal < darkLevel) && (ntVal > lghtLevel)) {
    sTar[0] = 'N'; sTar[1] = 'T'; 
    bRetVal = tNT;
  }
  else if ((vtVal > lghtLevel) && (ntVal < darkLevel)) {
    sTar[0] = 'V'; sTar[1] = 'T'; 
    bRetVal = tVT;
  }
  else if ((vtVal < darkLevel) && (ntVal < darkLevel)) {
    sTar[0] = 'E'; sTar[1] = 'D'; 
    bRetVal = tED;
  }
  else if ((vtVal > lghtLevel) && (ntVal > lghtLevel)) {
    sTar[0] = 'E'; sTar[1] = 'L'; 
    bRetVal = tEL;
  }
  else {
    sTar[0] = '-'; sTar[1] = '-'; 
    bRetVal = tUN;
  } 
  return(bRetVal); 
}  


boolean bofnIsNewHour(DateTime dt)
{
  if((dt.minute() == 0) && (dt.second() == 0)) {
    return(true);
  }
  else {
    return(false);
  }  
}


boolean bofnIsNewMinute(DateTime dt)
{
  if(dt.second() == 0) {
    return(true);
  }
  else {
    return(false);
  }  
}



//=================================================================================
void setup()
{
  // setup tasks ------------------------------------------------------------------
  stTasks[TASK_SECOND].iTaskID = TASK_SECOND;    // SECOND
  stTasks[TASK_SECOND].lPeriod = 500;            // 0.5 s
  stTasks[TASK_SECOND].lNextRun = 100;
  stTasks[TASK_SECOND].lDuration = 0;
  stTasks[TASK_SECOND].fnCallBack = &fnTask_SECOND;

  stTasks[TASK_DISPLAY].iTaskID = TASK_DISPLAY;  // display 
  stTasks[TASK_DISPLAY].lPeriod = 1000;          // 1 s
  stTasks[TASK_DISPLAY].lNextRun = 150;
  stTasks[TASK_DISPLAY].lDuration = 0;
  stTasks[TASK_DISPLAY].fnCallBack = &fnTask_DISPLAY;

  stTasks[TASK_GAS_BOILER].iTaskID = TASK_GAS_BOILER;  // gas boiler
  stTasks[TASK_GAS_BOILER].lPeriod = 1000;          // 1 s
  stTasks[TASK_GAS_BOILER].lNextRun = 180;
  stTasks[TASK_GAS_BOILER].lDuration = 0;
  stTasks[TASK_GAS_BOILER].fnCallBack = &fnTask_GAS_BOILER;

  stTasks[TASK_LOG].iTaskID = TASK_LOG;          // log   
  stTasks[TASK_LOG].lPeriod = 60000;             // 60 s
  stTasks[TASK_LOG].lNextRun = 200;
  stTasks[TASK_LOG].lDuration = 0;
  stTasks[TASK_LOG].fnCallBack = &fnTask_LOG;

#ifdef DEBUG_LIST
  stTasks[TASK_DEBUG].iTaskID = TASK_DEBUG;      // debug log to ser. line
  stTasks[TASK_DEBUG].lPeriod = 5000;            // 5 s
  stTasks[TASK_DEBUG].lNextRun = 250;
  stTasks[TASK_DEBUG].lDuration = 0;
  stTasks[TASK_DEBUG].fnCallBack = &fnTask_DEBUG;
#endif  

#ifdef MODBUS
  stTasks[TASK_MODBUS_ARCH].iTaskID = TASK_MODBUS_ARCH;    // archive retrieval
  stTasks[TASK_MODBUS_ARCH].lPeriod = 1000;                // 1000 ms
  stTasks[TASK_MODBUS_ARCH].lNextRun = 300;
  stTasks[TASK_MODBUS_ARCH].lDuration = 0;
  stTasks[TASK_MODBUS_ARCH].fnCallBack = &fnTask_MODBUS_ARCH;
#endif  

  stTasks[TASK_SYNCTIME].iTaskID = TASK_SYNCTIME;  // sync time from host
  stTasks[TASK_SYNCTIME].lPeriod = 10000;          // 10 s
  stTasks[TASK_SYNCTIME].lNextRun = 350;
  stTasks[TASK_SYNCTIME].lDuration = 0;
  stTasks[TASK_SYNCTIME].fnCallBack = &fnTask_SYNCTIME;
  //-----------------------------------------------------------------------------
#ifdef MODBUS  
  modbus_configure(&Serial, 19200, SERIAL_8N1, stationID, dataEnable, HOLDING_REGS_SIZE, holdingRegs); 
#endif  
#ifdef DEBUG_LIST  
  Serial.begin(19200);                           // serial line for debug logging
#endif  
  pinMode(ledPIN, OUTPUT);
  pinMode(pulsePIN, INPUT);
  pinMode(boilerPIN, INPUT_PULLUP);
  analogReference(DEFAULT);
  lcd.begin(16, 2);
  lcd.clear();                                   // start with a blank screen
  lcd.setCursor(0,0);  
  Wire.begin(); 
  if (! RTC.isrunning()) {
    // Serial.println("RTC is NOT running!");
    // following line sets the RTC to the date & time this sketch was compiled
    // RTC.adjust(DateTime(__DATE__, __TIME__));
  }
  attachInterrupt(pulseInterrupt, interruptHandler, FALLING);  
  curTime = RTC.now();
  fnDisplayDateTime(curTime, true);
  
  lcd.setCursor(0,1);  
  lcd.print("                ");
  fnClearRegs();
  delay(1000);
}  


//============================================================================
void loop()
{
  fnScheduler();
#ifdef MODBUS  
  modbus_update();
#endif  
}  
//============================================================================


//============================================================================
void interruptHandler() // routine called when external interrupt is triggered
{
  unsigned long lNow = millis();
  if ((lNow - lLastInt) > DEBOUNCE_LIMIT) { 
    nPulseCounter++;      //Update number of pulses, 1 pulse = 2Wh
    lLastInt = lNow;
  }  
}


//============================================================================
void fnScheduler()
{
  unsigned long lNow = millis();
  unsigned long lTmp;
  
  for(int i=0; i<NUMBER_OF_TASKS; i++) {
    if(stTasks[i].lNextRun <= lNow) {
      stTasks[i].lNextRun += stTasks[i].lPeriod; 
      stTasks[i].lLastRun = lNow; 
      lTmp = micros();
      stTasks[i].fnCallBack(stTasks[i].iTaskID);
      stTasks[i].lDuration = micros()-lTmp;
    }  
  }
}

//============================================================================
// Tasks
//============================================================================
void fnTask_SECOND(int iID)          // blink second collon 
{
  static char c = ' ';
  
  lcd.setCursor(8,0);  
  if(c == ' ') {
    c = ':';
  }
  else {
    c = ' ';
  }  
  lcd.print(c);
}

//----------------------------------------------------------------------------
void fnTask_DISPLAY(int iID)         // show datetime
{
  static boolean boFirst = true;
  unsigned long lUT;
  
  curTime = RTC.now();
  fnDisplayDateTime(curTime);
  ntVal=analogRead(ntPIN);
  vtVal=analogRead(vtPIN);
  bTarif = bfnGetTarif(sTarif);
  lcd.setCursor(14,0);
  lcd.print(sTarif);
  lcd.setCursor(13, 0);
  if(holdingRegs[GAS_BOILER_STATUS] == 1) {
    lcd.print('g');
  }  
  else {  
    lcd.print(' ');
  }
  if(boSaving) {
    lcd.setCursor(12, 0);
    lcd.print(' ');
    boSaving = false;
  }
  holdingRegs[TARIF] = bTarif;
  lUT = curTime.unixtime();
  holdingRegs[A_TIME_LW] = lUT;
  holdingRegs[A_TIME_HW] = lUT >> 16;
  if(bofnIsNewHour(curTime) || boFirst) {
    boFirst = false;
    fEnergyNTlst = fEnergyNT;
    fEnergyVTlst = fEnergyVT;
    fEnergyNT = 0;
    fEnergyVT = 0;
    lcd.setCursor(0,1);
    lcd.print('N');
    lcd.print(fEnergyNTlst, 1); // kWh
    lcd.print(' ');
    lcd.print('V');
    lcd.print(fEnergyVTlst, 1); // kWh
    holdingRegs[LT_ENERGY_LST] = (int)(round(fEnergyNTlst*100));
    holdingRegs[HT_ENERGY_LST] = (int)(round(fEnergyVTlst*100));
    holdingRegs[GAS_BOILER_TIME] = (int)(round(iGasBoilerTimeOn/60));
    iGasBoilerTimeOn = 0;
  }  
  if(bofnIsNewHour(curTime)){
    fnAddToArchive(lUT, holdingRegs[LT_ENERGY_LST], holdingRegs[HT_ENERGY_LST]);
    holdingRegs[ARCH_DEPTH] = bfnGetArchiveLen();
  }  
}  

//----------------------------------------------------------------------------
void fnTask_LOG(int iID)         // log
{
  char sNum[6];

  lcd.setCursor(12, 0);
  lcd.print('*');
  fnGetCurPower();
  fnAlignNum(iCurPower, 5, sNum);
  lcd.setCursor(10,1);
  lcd.print(sNum);
  lcd.print('W');
  boSaving = true;
  holdingRegs[POWER] = iCurPower;   
  holdingRegs[LT_ENERGY_CUR] = (int)(round(fEnergyNT*100));
  holdingRegs[HT_ENERGY_CUR] = (int)(round(fEnergyVT*100));
}  


//----------------------------------------------------------------------------
void fnTask_DEBUG(int iID)        // debug print to serial line
{
  struct Task* pPtr;
  
  for(int i=0; i<NUMBER_OF_TASKS; i++) {
    pPtr =  &stTasks[i];
#ifdef DEBUG_LIST    
    Serial.print(i);
    Serial.print(":");
    Serial.print(pPtr->lPeriod);
    Serial.print(":");
    Serial.print(pPtr->lLastRun);
    Serial.print(":");
    Serial.print(pPtr->lNextRun);
    Serial.print(":");
    Serial.println(pPtr->lDuration);
#endif    
  }  
#ifdef DEBUG_LIST    
  Serial.println(curTime.unixtime());
#endif   
#ifdef MEMORY_TEST
  Serial.print("Free memory: ");
  Serial.println(freeMemory());
#endif
}


//----------------------------------------------------------------------------
void fnTask_MODBUS_ARCH(int iID)        // dispatch modbus protocol
{
  unsigned long lRqTime;
  unsigned long lRsTime;
  unsigned long lTmp;
  byte bPos;
  
  lTmp = (unsigned long)holdingRegs[RQ_HIST_TIME_HW];
  lRqTime = lTmp << 16;
  lRqTime += holdingRegs[RQ_HIST_TIME_LW];
  
  lTmp = (unsigned long)holdingRegs[RS_HIST_TIME_HW];
  lRsTime = lTmp << 16;
  lRsTime += holdingRegs[RS_HIST_TIME_LW];
  
  if(lRqTime != lRsTime) {
    bPos = bfnGetArchPosition(lRqTime);
    //bPos = 1; // for test only
    if (bPos <= ARCHIVE_SIZE) {
      holdingRegs[RS_HIST_HT_ENER] = stArchive[bPos].nEnergyHT;
      holdingRegs[RS_HIST_LT_ENER] = stArchive[bPos].nEnergyLT;
      holdingRegs[RS_HIST_TIME_HW] = lRqTime >> 16;
      holdingRegs[RS_HIST_TIME_LW] = lRqTime;
    } 
    else {
      holdingRegs[RS_HIST_HT_ENER] = 0;
      holdingRegs[RS_HIST_LT_ENER] = 0;
      holdingRegs[RS_HIST_TIME_HW] = 0;
      holdingRegs[RS_HIST_TIME_LW] = 0;
    }  
  }
}  

//----------------------------------------------------------------------------
void fnTask_SYNCTIME(int iID)      // sync time from master
{                                  // sync if sync flag is not zero
  DateTime dtMaster;
  unsigned long lMasterTime;
  unsigned long lTmp;
  
  if(holdingRegs[STIME_FLAG] != 0) {
    lTmp = (unsigned long)holdingRegs[M_TIME_HW];
    lMasterTime = lTmp<<16;
    lMasterTime += holdingRegs[M_TIME_LW];
    dtMaster = DateTime(lMasterTime);
    RTC.adjust(dtMaster);    
    holdingRegs[STIME_FLAG] = 0;
  }  
}  

//----------------------------------------------------------------------------
void fnTask_GAS_BOILER(int iID)    // get gas boiler status
{
  if(digitalRead(boilerPIN)) {     // low means on 
    holdingRegs[GAS_BOILER_STATUS] = 0;
  }
  else{
    holdingRegs[GAS_BOILER_STATUS] = 1;
    iGasBoilerTimeOn++;
  }
}