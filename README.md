Enemon
======

Monitoring of electrical energy consumption 

Components

- empoint - communication master with simple ncurses UI
- clients 
  - arduino clients for data gathering and control
  - rpi client with same functionality based on twisted python
- web - web application, shows current data and statistics
- some new information
